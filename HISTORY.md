# History of versions #

* version 0.13 (2021-06-08)
  * Now manages correctly the Duff's Device when there is an IF ... ELSE ... that crosses a CASE

* version 0.12 (2021-05-26)
  * Static variables are managed like global ones

* version 0.11 (2020-10-02)
  * Fixed bug when having nested blocks in protothreads

* version 0.10 (2020-10-01)
  * Added support for protothread structure

* version 0.9 (2019-01-06)
  * Added support for _Static_assert
  * Added preliminary support for Statements and Declarations in Expressions

* version 0.8 (2018-12-25)
  * Add support for CONST and RESTRICT statements in pointers
  * Add support for array syntax in function declaration parameters

* version 0.7 (2018-12-24)
  * Now doesn't return an error when returning NULL as borrowed

* version 0.6 (2018-10-03)
  * Fixed case when a switch/case block lacks a 'default' statement

* version 0.5 (2018-10-01)
  * Only checks function declarations in external files if their definition is in the current file

* version 0.4 (2018-09-27)
  * Forbides to define functions with empty parameters list; they must have, in case they don't need parameters, 'void'

* version 0.3 (2018-08-25)
  * Allows to generate the *crust.h* file just calling crust, instead of having to copy it from elsewhere

* version 0.2 (2018-08-23)
  * First public version
