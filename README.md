# CRUST

CRUST is a C static analyzer that allows to have in C some of
the memory checks available in RUST, allowing to create more
secure code without the overhead of libraries or other
programming techniques like reference counters or garbage
collectors.

CRUST works by adding several tags to the source code. These
tags allow to specify which pointers must be tracked by CRUST
and their properties.

The tags are automagically removed by the C Preprocessor during
compilation, so they are completely transparent to the compiler.
That also means that there are no fancy libraries, runtimes or
expandable macros: the source code is just standard, plain C.
The tags just allow to annotate specific information for the
CRUST static analyzer, but they aren't needed or used during
normal compilation.

It can be useful for projects where RUST is not feasible, like
code for microcontrollers, kernel drivers...

## INSTALLATION

In order to install and use CRUST, you need *python 3*, *gcc*,
*flex* and *bison*.

After downloading the source code, just run:

        sudo ./setup.py install

It will compile the *flex* and *bison* code, generate the main
library with the parser, and install the static analyzer.

## USAGE

For detailed documentation, check the *doc* folder.

crust [-I include_path] [-I...] [-D define] [-D...] [--include filename] [--include...] [--nowarnings] [--nocpp] [--quiet] [-e exclude_filename] [--debug1=nline] [--debug2=nline] file.c [file.c] ...

* -I             allows to specify paths where the C preprocessor will search for header files. Can be repeated as many times as paths have to be added
* -D             allows to specify #defines from command line that the C preprocessor must consider defined. Can be repeated as many times as defines are needed
* --include       allows to include a file in the final preprocessed code. Can be repeated as many times as defines are needed
* --nowarnings   will hide the WARNING messages, showing only ERROR and CRITICAL messages
* --nocpp        will pass the source code directly to the parser, instead of pass it before through the C preprocessor
* --quiet        will not show extra information
* -e             allows to specify file names to exclude. This is useful when a filename contains wildcards
* --preprocessed stores the preprocessed code (this is, after being passed through the C PreProcessor) with the specified filename
* --debug1 & --debug2 shows the parsed tree between source code lines specified in both statements
* file.c        the path to the file or files to analyze. It can contain wildcards, and it is possible to also specify several files

crust --headers

* will generate the file *crust.h*, needed to compile any source or header file that contains crust qualifiers, in the current folder.

## CONTACTING THE AUTHOR

Sergio Costas Rodriguez  
rastersoft@gmail.com  
http://www.rastersoft.com  
https://gitlab.com/rastersoft/crust
