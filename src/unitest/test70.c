typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t __crust_borrow__ param) {

	crust_t var2;

	var2 = 1 ? param : NULL; // Assigning a borrowed block to a local variable is not allowed
	__crust_disable__ // added here to ensure only one error per unitest
}
