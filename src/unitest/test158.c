__crust_borrow__ int *global_var; // ERROR: global variables can't be borrowed

void main(void) {
	__crust_borrow__ int *var; // It is valid to create local borrowed variables
}
