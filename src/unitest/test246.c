typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t __crust_borrow__ function2(void);

crust_t __crust_borrow__ function1(crust_t __crust_borrow__ param1) {
	crust_t __crust_borrow__ tmp;

	tmp = function2();

	return tmp; // Valid
}
