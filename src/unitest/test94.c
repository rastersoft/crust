typedef __crust__ unsigned char *crust_t;

typedef crust_t (*prueba)();

#define NULL ((void *)0)

crust_t function(void);

void main(void) {
	prueba test1 = function;
	(*test1)(); // ERROR: doesn't store the return value
}
