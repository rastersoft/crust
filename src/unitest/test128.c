typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {

	crust_t __crust_alias__ alias1;
	crust_t __crust_alias__ alias2;

	__crust_debug__ // here, 'param' must be NOT_NULL_OR_NULL, and 'alias1' and 'alias2' must be UNINITIALIZED
	alias1 = param;
	__crust_debug__ // here, 'param' and 'alias1' must be NOT_NULL_OR_NULL, and 'alias2' must be UNINITIALIZED
	alias2 = alias1;
	__crust_debug__ // here, 'param', 'alias1' and 'alias2' must be NOT_NULL_OR_NULL
}
