typedef __crust__ unsigned char *crust_t;

int function(void) {

	crust_t retval = function(); // Error: assigning non-crust return value to a crust variable
	return 5;
}
