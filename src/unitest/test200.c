struct crust_ts {
    char var1;
    int var2;
    struct crust_ts *next;
};

typedef __crust__ struct crust_ts* crust_t;

crust_t global_var;

void function(crust_t);

void main(crust_t __crust_not_null__ var) {

    global_var.var1 = var; // this is fine
    __crust_debug__
}
