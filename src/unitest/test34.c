typedef __crust__ unsigned char *crust_t;

void main(void) {

	crust_t a, b;
	a = b = 5; // ERROR: assigning a non-crust value to two crust variables
	// ERROR 'a' is considered initialized, but it is not used
}
