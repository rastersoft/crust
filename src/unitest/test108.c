typedef __crust__ unsigned char *crust_t;

typedef int (*prueba)(unsigned char *, int);

#define NULL ((void *)0)

int function(crust_t, int);

void main(void) {
	prueba test1 = function; // ERROR: the first parameter doesn't match between both function definitions
}
