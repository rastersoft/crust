typedef __crust__ unsigned char *crust_t;

typedef char* (*prueba)(crust_t);

#define NULL ((void *)0)

void main(crust_t param1) {
	prueba test1 = NULL;
	(*test1)(param1); // ERROR: calling a function pointer with a NULL value
}
