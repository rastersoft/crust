typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {
	main(NULL);
	// ERROR: 'param' is not managed at the end of the non-empty block
}
