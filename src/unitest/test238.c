typedef __crust__ unsigned char *crust_t;

void function(crust_t);

void main(crust_t p) {

    // Here, p in in NOT_NULL_OR_NULL state
    function(p);
    // Now, p is in FREED_OR_NULL state

    if (!p) { // NO ERROR, because we are just comparing
        p = NULL;
    }
}
