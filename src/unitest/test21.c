typedef __crust__ unsigned char *crust_t;

int function2(crust_t param1);

void function(crust_t param1, crust_t param4) {

	int retval = function2(param4);
	retval = (param4 == param1) ? 1 : 0; // NO ERROR: param4 has been freed, but still can be compared
	retval = function2(param1);
}
