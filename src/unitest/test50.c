typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void test_function(crust_t param);

void main(crust_t __crust_borrow__ param) {
	test_function(param); // ERROR: can't pass a borrowed variable as a non-borrow parameter
}
