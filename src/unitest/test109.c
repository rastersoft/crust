typedef __crust__ unsigned char *crust_t;

typedef int (*prueba)(crust_t, int);

#define NULL ((void *)0)

crust_t function(crust_t, int);

void main(void) {
	prueba test1 = function; // ERROR: the first parameter doesn't match between both function definitions
}
