typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t __crust_borrow__ function1(void);

void main(void) {

	crust_t var1;

	var1 = function1(); // ERROR: assigning a borrowed block to a non-borrow variable
}
