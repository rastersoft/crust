typedef __crust__ unsigned char *crust_t;

crust_t var1;

void main(void) {

    if (var1 == NULL) {
        var1 = NULL; // just to access the variable
    }
    __crust_set_not_null__(var1); // this is not allowed because we already have accessed the globar variable
}
