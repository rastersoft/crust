typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t function(void);

crust_t  main(void) {

	return function(); // this is fine.
}
