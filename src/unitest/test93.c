typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function(void);

void main(void) {
	void (*test1)() = function;
	(*test1)(); // all fine
}
