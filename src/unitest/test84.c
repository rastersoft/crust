typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t param1) {
	(*test1)(param1); // ERROR: calling an undefined function pointer
}
