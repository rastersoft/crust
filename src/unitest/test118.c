typedef __crust__ unsigned char *crust_t;

typedef char* (*prueba)(crust_t);

#define NULL ((void *)0)

prueba function(void);

void main(crust_t param1) {
	prueba test1 = function();
	(*test1)(param1); // WARNING: calling a function pointer with a possible NULL value
}
