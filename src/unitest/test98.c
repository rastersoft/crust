typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t function(void);

void main(crust_t param1, crust_t param2) {

	if ((param1 == NULL) && (param2 == NULL)) {
		param1 = function();
		param2 = function();
	} // ERROR: param1 and param2 aren't freed, nor when initialized at line 7, neither at lines 10 and 11
}
