typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

int main(int param) {

	return param; // All fine: expects a non-crust value, and a non-crust value is returned
}
