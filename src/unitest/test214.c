void main(void) {
	struct test_t {
		int v;
		char b;
	} test = {
		.v = 1,
		.b = 0
	};

	int f;

	test.v++;
	f = test.b; // all fine
}
