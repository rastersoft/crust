typedef __crust__ unsigned char *crust_t;

struct {
	crust_t value;
} test;

void function(crust_t param3) {
	test.value = param3; // this is legal
}
