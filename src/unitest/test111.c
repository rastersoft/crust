typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t function(void);

void main(void) {

	crust_t param;
	int b;

	param = (b=6, function()); // this is legal, because the return value of function is being assigned to "param"
}
