typedef __crust__ int *crust_t;

#define NULL ((void *)0)

crust_t function(crust_t param);

void main(crust_t param) {

	crust_t __crust_alias__ an_alias = param;
	__crust_debug__
	an_alias = function(param); // ERROR: it is not allowed to assign the return block of a function to an alias
	__crust_debug__
}
