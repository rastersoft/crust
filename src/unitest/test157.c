enum {
    STATUS1, STATUS2
} status = STATUS1;

void main(int b) {

	enum {
		MORE1, MORE2
	} more = MORE1;

	status = STATUS2;
	more = MORE2;
}
