typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t param1) {
	void (*test1)(crust_t) = main;
	(*test1)(param1); // all fine
}
