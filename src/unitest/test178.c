typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t __crust_not_null__ main(void) {

	return NULL; // ERROR: the return value can't be NULL
}
