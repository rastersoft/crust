typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t __crust_not_null__ function1(void);

void function2(crust_t);

void main(crust_t param) {

	crust_t var;
	var = function1();
	if (var == NULL) {
		return; // if this is run, there will be an error because PARAM won't be freed
	}
	function2(var);
	function2(param); // All fine. Since function1 return can't be NULL, this code will be called always, and the return in line 14 will never be called
}
