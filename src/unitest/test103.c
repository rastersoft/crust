typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function(crust_t param);

void main(int b) {

	crust_t param;

	while(b) {
		param = (crust_t) 1;
		break;
	}
	function(param); // ERROR: the while can be run zero times, so param can be uninitialized
}
