#define NULL ((void *)0)

struct crust_ts {
    char var1;
    int var2;
    struct crust_ts *next;
};

typedef __crust__ struct crust_ts* crust_t;

void use_crust_var(crust_t);
crust_t recycle_crust_var(crust_t);

void main(crust_t var) {

    int a = 5;

    switch(a) {
        default:
        while(1) {
            use_crust_var(var);
            return;
        case 1:
            var = recycle_crust_var(var);;
            a++;
        }
    }
}
