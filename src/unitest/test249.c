// not allowed: C doesn't check that calls to functions without parameters honors the signature
// Thus, it is possible to call this function with several parameters, or assign it to a pointer
// to function with any type and number of parameters.

// To avoid this, it is a must to add "void" inside the parameter list;
// this is, the right definition is:
// int main(void) {

int main() {
    return 0;
}