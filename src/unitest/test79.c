typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t param1) {
	char* (*test1)(crust_t) = NULL;
	(*test1)(param1); // ERROR: calling a function pointer with a NULL value
}
