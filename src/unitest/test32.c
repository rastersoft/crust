typedef __crust__ unsigned char *crust_t;

void function(int param3) {

	crust_t param = NULL;
	function(param); // ERROR: passing a crust variable as a non-crust parameter
}
