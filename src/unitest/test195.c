#define NULL ((void *)0)

__crust__ struct crust_t2 {
    char var1;
    int var2;
    __crust__ struct crust_t2 *next;
};

void main(__crust__ struct crust_t2 *var) {

    __crust__ struct crust_t2 *tmp;
    for(; var != NULL; var = tmp) {
        tmp = var->next;
    }

}
