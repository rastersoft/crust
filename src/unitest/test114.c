void main(void) {

	int b = 1;

	b += 6;
	b -= 2;
	b *= 4;
	b /= 2;
	b %= 2;
	b &= 3;
	b |= 7;
	b ^= 9;
	b >>= 2;
	b <<= 3; // all fine, just a check for these ops
}
