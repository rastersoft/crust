typedef __crust__ unsigned char *crust_t;

typedef int (*prueba)();

#define NULL ((void *)0)

int function(int);

void main(void) {
	prueba test1 = function; // this is legal for CRUST because there are no crust-type parameters involved
}
