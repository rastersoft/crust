typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function(crust_t param);

void main(int b) {

	crust_t param;

	do {
		param = (crust_t) 1;
		break;
	} while(b);
	function(param);
}
