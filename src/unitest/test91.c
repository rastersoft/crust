typedef __crust__ int *crust_t;

#define NULL ((void *)0)

void main(void) {

	crust_t tmp1;

	{
		crust_t tmp1 = NULL;
		__crust_debug__ // here tmp1 must be at state NULL
	}
	__crust_debug__ // but here it must be at state UNINITIALIZED, because it is outside the block
	// And all fine
}
