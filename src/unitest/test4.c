typedef __crust__ unsigned char *crust_t;

int function(crust_t);

void main(void) {
	crust_t param4;
	int retval = function(param4); // ERROR: param4 is used before being assigned
}
