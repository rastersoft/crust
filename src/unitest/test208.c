typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function(crust_t);

void main(crust_t param1, crust_t param2) {
    static int a = 0;
    __crust_debug__
    if (a == 0) {
        function(param1);
    } else {
        function(param2);
    }
}
