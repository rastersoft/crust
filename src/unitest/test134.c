typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t main(crust_t param) {

	return param; // All fine: expects a crust value, and a crust value is returned
}
