typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {

	int b = 0;

	do {
		main(param);
	} while (b == 0);
	// ERROR: since b is zero, param will never be freed
}
