typedef __crust__ unsigned char *crust_t;

void function2(crust_t param);

void function(void) {
	crust_t param4 = NULL;

	int retval = (param4 == NULL) ? 1 : 0; // It is fine to just check if the variable is NULL or not NULL; if it is used after this, it will fail there
	function2(param4);
}
