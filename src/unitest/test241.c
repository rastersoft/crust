typedef __crust__ unsigned char *crust_t;

void function(crust_t);

void main(crust_t evento) {

    function(evento);

    if (evento != NULL) { // Valid: now evento is freed, but it still contains a pointer that can be compared with NULL
        evento = NULL;
    }
}