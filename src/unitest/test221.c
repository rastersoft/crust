#define NULL ((void *)0)

void *calloc(int nmemb, int size);

struct crust_ts {
    char var1;
    int var2;
    struct crust_ts *next;
};

typedef __crust__ struct crust_ts* crust_t;

void function(void) {

    crust_t var = NULL;

    var = (crust_t) calloc(1, 800);
}
