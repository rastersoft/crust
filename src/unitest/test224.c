typedef __crust__ unsigned char *crust_t;

void test_function(unsigned char *param); // this one must be ignored
__crust_override__ void test_function(crust_t __crust_borrow__ param);

#define NULL ((void *)0)

void main(crust_t __crust_borrow__ param) {
	test_function(param);
}
