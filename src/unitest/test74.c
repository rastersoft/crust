typedef __crust__ int *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {

	crust_t __crust_alias__ an_alias = param;

	main(param); // this must work, but now "an_alias" must be marked as "free"
	main(an_alias); // ERROR: 'an_alias' has been freed when the base variable "param" was used at line 9
}
