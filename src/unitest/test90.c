typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function(int);

void main(crust_t param1) {
	void (*test1)(int) = function;
	(*test1)(param1); // ERROR: calling a function pointer with incorrect type of parameters
}
