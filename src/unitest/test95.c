typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t function(void);

void main(void) {
	crust_t (*test1)() = function;
	(*test1)(); // ERROR: doesn't store the return value
}
