typedef __crust__ int *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {

	crust_t __crust_alias__ an_alias = param;
	main(an_alias); // this must work, but now "param" must be marked as "free"
	main(param); __crust_no_warning__ // ERROR: param has been freed when the alias "an_alias" has been used
	                                  // and being an ERROR, __crust_no_warning__ must have no effect
}
