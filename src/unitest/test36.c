typedef __crust__ unsigned char *crust_t;

struct {
	crust_t value;
} test;

void function2(crust_t);

void function(void) {
	crust_t param4 = test.value; // this is legal
	function2(param4);
}
