typedef __crust__ unsigned char *crust_t;

typedef void (*prueba)();

#define NULL ((void *)0)

void function(void);

void main(void) {
	prueba test1 = function;
	(*test1)(); // all fine
}
