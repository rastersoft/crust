typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function1(crust_t __crust_not_null__ param);

void main(void) {

	function1(NULL); // ERROR: the parameter must be not_null
}
