typedef __crust__ unsigned char *crust_t;

struct {
	crust_t value;
} *test;

void function(crust_t param3) {
	test = param3; // ERROR: assigning a crust element to a non-crust variable
}
