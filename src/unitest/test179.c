typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t __crust_not_null__ main(crust_t param) {

	return param; // ERROR: param can be NULL, but the return value can't
}
