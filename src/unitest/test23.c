typedef __crust__ int * crust_t;

void function(void) {
	crust_t param4;
	crust_t param5 = param4; // ERROR: assigning an uninitialized variable
}
