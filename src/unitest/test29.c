typedef __crust__ unsigned char *crust_t;

int function(crust_t);

void main(void) {
	crust_t param4;
	function(param4); // ERROR: calling a function with an uninitialized parameter
}
