typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void test_function(crust_t __crust_borrow__ param) {
}

void main(crust_t __crust_borrow__ param2) {
	void (*test2)(crust_t __crust_borrow__ param3);
	test2 = test_function; // this is legal
}
