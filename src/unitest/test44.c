typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {

	crust_t var2 = (crust_t) 5;

	if (param == NULL) {
		param = var2; // this is legal (assigning an object to a NULL variable)
	}
	// ERROR: param isn't freed in none of the two points where it is initialized (line 5 and line 10).
	// ERROR: var2 isn't freed (if param is not NULL, it won't be assigned var2)
}
