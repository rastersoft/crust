typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {

	crust_t var2;
	var2 = param;
	main(var2);
	return;
	main(param); // this won't cause an error because it will never be executed
}
