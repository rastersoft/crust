typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t __crust_borrow__ function1(crust_t param1) {
	return param1; // ERROR: a non-borrowed parameter can't be returned as a borrowed
}
