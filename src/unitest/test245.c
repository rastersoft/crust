typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t __crust_borrow__ function1(void);

void function2(crust_t __crust_borrow__);

void main(void) {

	__crust_borrow__ crust_t var1;
	__crust_borrow__ crust_t var2;

	var1 = function1(); // Valid
	var2 = var1; // since both are borrowed, this assignment doesn't invalidates var1
	function2(var1);
	function2(var1);
	function2(var2);
	function2(var2);
}
