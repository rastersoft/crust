typedef __crust__ unsigned char *crust_t;

void function(crust_t);

void main(void) {

	int *param = 1;
	int retval = function(param); // ERROR: passed non-crust variable as a crust parameter
}
