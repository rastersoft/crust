typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t function(crust_t __crust_recycle__);
void function2(crust_t);

void main(void) {

	crust_t var2 = (crust_t) 5;
	crust_t var3;
	var3 = function(var2); // This is legal
	__crust_debug__
	function2(var3);
	__crust_debug__
}
