#define NULL ((void *)0)

struct crust_ts {
    char var1;
    int var2;
    struct crust_ts *next;
};

typedef __crust__ struct crust_ts* crust_t;

void function2(crust_t __crust_borrow__);

void function(crust_t var) {

    crust_t __crust_alias__ tmp1;
    for(tmp1 = var; tmp1 != NULL; tmp1 = tmp1->next) {
        function2(tmp1);
    }
}
