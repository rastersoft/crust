typedef __crust__ unsigned char *crust_t;

struct {
	crust_t value;
} *test;

void function(crust_t);

void main(void) {
	crust_t param4 = test->value; // this is legal
	function(param4);
}
