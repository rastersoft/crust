typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function(__crust_borrow__ crust_t, int, ...);

void main(crust_t param) {

	function(param,1);
	function(param,1,2);
	function(NULL,1,param); // ERROR: crust-type params can't be used in variable lengh parameters
}
