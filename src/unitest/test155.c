typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function(crust_t param);

void main(int b) {

	crust_t param;

	while __crust_no_0__ (b) {
		param = (crust_t) 1; // ERROR: the second time the loop is run, this will overwrite the old value
		continue;
		function(param); // this will never be called
	}
	function(param); // This will always work because we disabled the option of never execute the statements inside the loop
}
