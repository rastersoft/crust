typedef __crust__ unsigned char *crust_t;

crust_t function(void);

void main(void) {

	function(); // ERROR: the function returns a crust_t pointer, but it isn't stored
}
