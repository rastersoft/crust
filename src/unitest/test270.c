typedef __crust__ unsigned char *crust_t;

crust_t function(void);

void main(void) {

    static crust_t p;
    __crust_set_null__(p);
    p = function();
}
