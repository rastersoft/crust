typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void test_function(crust_t __crust_borrow__ param) {
}

void main(crust_t __crust_borrow__ param) {
	void (*test2)(crust_t param) = NULL;
	test_function = test2; // ERROR: assigning a value to a function name
}
