typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function(crust_t param);

void main(int b) {

	crust_t param;

	__crust_no_0__ while(b) {
		param = (crust_t) 1;
		break;
	}
	function(param); // All fine: thanks to __crust_no_0__, the while is executed at least once, so param is always assigned
}
