typedef __crust__ unsigned char *crust_t;

typedef crust_t (*prueba)();

typedef struct {
	prueba function_p;
} tmp_struct;

#define NULL ((void *)0)

crust_t function(void);

void main(void) {
	tmp_struct data1 = 1; // to ensure that it is not NULL
	data1.function_p = function; // This is legal
}
