typedef __crust__ unsigned char *crust_t;

void function(crust_t param1) {
	int retval = (param1 == NULL) ? 1 : 0;
	param1 = NULL; // ERROR: param1 should still have a valid value, because the comparison doesn't free it
}
