typedef __crust__ unsigned char *crust_t;

void function(void) {

	int v = 5;
	crust_t param = (crust_t) v; // This is legal only in some cases; that's why it will show a Warning
	// ERROR: param is still in use at the end of the function
}
