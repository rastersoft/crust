__crust__ struct t_crust_t {
	int value;
	__crust__ struct t_crust_t *next;
}

typedef struct t_crust_t  *crust_t;

#define NULL ((void *)0)

void function(crust_t);

void main(crust_t param1, crust_t __crust_not_null__ param2) {

	crust_t __crust_alias__ alias1;
	crust_t __crust_alias__ alias2;

	__crust_debug__ // here, 'param1' must be NOT_NULL_OR_NULL, 'param2' must be NOT_NULL, 'alias1', 'alias2' must be UNINITIALIZED
	alias1 = param1;
	__crust_debug__ // here, 'param1', 'alias1' must be NOT_NULL_OR_NULL and share UID, 'param2' must be NOT_NULL, 'alias2' must be UNINITIALIZED
	alias2 = param2;
	__crust_debug__ // here, 'param1', 'alias1' must be NOT_NULL_OR_NULL and share UID, 'param2', 'alias2' must be NOT_NULL and share UID

	alias2->next = alias1;
	__crust_debug__ // here 'param1', 'alias1' must be FREED_OR_NULL, 'param2', 'alias2' must be NOT_NULL and share UID
	function(param2);
	__crust_debug__ // here 'param1' and 'alias1' must be FREED_OR_NULL, and 'param2', 'alias2' must be FREED
}
