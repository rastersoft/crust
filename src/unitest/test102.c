typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {

	while(param == NULL) {
		param = (crust_t) 1;
	}
	main(param); // All fine: since param will be assigned only as long as it is NULL, there are no errors
}
