typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function(crust_t param3) {

	__crust_alias__ crust_t var1;
	var1 = param3; // ERROR: it is assigned to an alias, but it is a local one
}
