typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t __crust_borrow__ param) {

	param = NULL; // ERROR: a borrowed parameter can't be overwritten
}
