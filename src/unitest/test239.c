typedef __crust__ unsigned char *crust_t;

void function(crust_t);

void main(void) {

    crust_t p = NULL;
    // Here, p in in NULL state

    function(p);

    if (!p) { // NO ERROR; p is NULL
        p = NULL;
    }
}
