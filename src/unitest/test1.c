#define NULL ((void *)0)

typedef __crust__ struct {
	int member;
	int p1;
	int p2;
} *crust_t;

__crust__ unsigned char * calling(__crust__ unsigned char *param1, __crust_recycle__ unsigned char *param2, crust_t param3);
__crust__ unsigned char * calling2(void);

__crust__ unsigned char * calling3(void) {

	return NULL;
}

void calling4(crust_t param);

void calling5(crust_t __crust_borrow__ param) {
	int a = 5;
	int b = a > 6 ? param->p1 : param->p2;
}

void calling6(int v);

void main(void) {

	int a = 1 ? 3 : 0;

	crust_t param1 = NULL;
	crust_t param2 = (crust_t) "hello world";
	crust_t param3 = NULL;
	__crust_debug__

	calling6(param2->member);

	int tmp = param2->member;

	crust_t param4;

	param4 = calling(param1,param2,param3);
	param1 = NULL;
	param2 = NULL;
	param3 = NULL;

	param1 = calling2();
	param2 = calling3();
	calling4(param1);
	calling4(param2);
	calling4(param4);
}
