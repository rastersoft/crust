void function(void (*cb)(void)) {
    cb(); // must produce an error (cb can be NULL)
}
