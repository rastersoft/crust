typedef __crust__ unsigned char *crust_t;

void test_function(crust_t __crust_borrow__ param);
void test_function(unsigned char *param); // this one must fail because it is repeated and the parameters doesn't match

#define NULL ((void *)0)

void main(crust_t __crust_borrow__ param) {
	test_function(param); // this one must not fail
}
