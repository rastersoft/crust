#define NULL ((void *)0)

struct crust_ts {
    char var1;
    int var2;
    struct crust_ts *next;
};

typedef __crust__ struct crust_ts* crust_t;

crust_t replace_crust_var(crust_t);

int main(crust_t var) {

    int a = 5;

    while(1) {
        var = replace_crust_var(var);
    }
    return 0;
}
