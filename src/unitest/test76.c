typedef __crust__ int *crust_t;

#define NULL ((void *)0)

void function(crust_t param);

void main(crust_t param1, crust_t param2) {

	crust_t __crust_alias__ an_alias = param1;
	an_alias = param2;

	function(param1); // this must work
	__crust_debug__ // deep 0; 'an_alias' must remain as NULL_OR_NOT_NULL
	function(param2); // this must work
	__crust_debug__ // deep 1; now 'an_alias' must be marked as "freed"
}
