typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {

	crust_t var2 = (crust_t) 5;
	if (param != NULL) {
		main(param);
	}
	param = var2; // if param was NULL, var2 can be assigned; if it wasn't, "main(param)" was called, so now it is FREED and can be assigned
	main(param);
}
