typedef __crust__ int *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {

	crust_t __crust_alias__ an_alias = param;
	__crust_debug__
	main(param); // this must work, but now "an_alias" must be marked as "freed"
	__crust_debug__
	an_alias = NULL; // since the alias has been marked as "freed", this must work: now 'an_alias' is decoupled from 'param' ('param' is still FREED)
	__crust_debug__
	main(param); // ERROR: 'an_alias' was decoupled at line 10, so 'param' is still FREED
}
