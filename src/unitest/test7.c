typedef __crust__ unsigned char *crust_t;

void function(int);

void main(void) {

	crust_t param = NULL;
	function(param); // ERROR: passed a crust variable to a non-crust param
}
