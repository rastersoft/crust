typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t function(void);

void main(crust_t param1, crust_t param2) {

	if ((param1 != NULL) && (param2 == NULL)) {
		param1 = function();
		param2 = function();
	} // All fine
}
