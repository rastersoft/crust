typedef __crust__ unsigned char *crust_t;

crust_t var1;

void function(crust_t);

void main(crust_t __crust_not_null__ arg1) {

    if (var1 != NULL) {
        var1 = arg1;
    } else {
        function(arg1);
    }
    // ERROR: can't assign a value to var1 when it is not NULL
}
