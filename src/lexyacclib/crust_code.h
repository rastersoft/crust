/* Copyright 2017 (C) Raster Software Vigo (Sergio Costas)
 *
 * This file is part of CRUST
 *
 * CRUST is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License.
 *
 * CRUST is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include <string.h>
#include <stdbool.h>

struct AST {
	int type;
	int line;
	char *filename;
	char *data;
	char *name;

	int pointer;
	bool t_const;
	bool t_restrict;
	bool t_volatile;

	bool t_void;
	bool t_char;
	bool t_short;
	bool t_int;
	bool t_long;
	bool t_longlong;
	bool t_float;
	bool t_double;
	bool t_signed;
	bool t_unsigned;
	bool t_bool;
	bool t_va_list;
	bool t_complex;
	bool t_imaginary;
	bool t_struct;
	bool t_union;
	bool t_enum;
	bool t_extern;
	bool t_typedef;
	bool t_static;
	bool t_auto;
	bool t_register;
	bool t_inline;
	bool function;
	bool t_crust;
	bool t_crust_borrow;
	bool t_crust_recycle;
	bool t_crust_alias;
	bool t_crust_no_zero;
	bool t_crust_not_null;
	bool t_null;
	bool t_ellipsis;
	bool t_label;
	bool t_override;

	long long int intval;
	double floatval;

	struct AST *next;
	struct AST *right;

	struct AST *child1;
	struct AST *child2;
	struct AST *child3;
	struct AST *child4;
	struct AST *condition;

	struct AST *function_params;
	struct AST *return_value;
	struct AST *bitfield;
	struct AST *struct_data;
	struct AST *enum_data;
	struct AST *arrays;
	struct AST *assignment;

	struct AST *for_ch1;
	struct AST *for_ch2;
	struct AST *for_ch3;
};

extern char *previous_data_line;
extern char *current_data_line;
extern int current_line;
extern char* current_file;
extern int debug_line1;
extern int debug_line2;
extern struct AST *types_list;
extern char *token_list[];
extern struct AST *full_tree;
extern char do_verbose;
extern int column;

void set_code_tree(struct AST *tree);

struct AST *find_type(char *type);


int prepare_leaf(unsigned long long int, double, int);

struct AST * new_leaf(int type, struct AST *template);
struct AST * new_leaf_char(int type, char *data);
void append_right(struct AST *leaf, struct AST *right);
void append_next(struct AST *leaf, struct AST *next);
void append_next_with_blocks(struct AST *leaf, struct AST *next);
void append_next_leaf(struct AST *leaf,int type_leaf, int line);
void append_fparams(struct AST *leaf, struct AST *param);
void mix_ast_leaves(struct AST *base, struct AST *new_leaf);
void copy_ast_data(struct AST *base, struct AST *new_leaf);
struct AST *transform_leaf(struct AST *leaf, int newtype);
void free_tree(struct AST *tree);
void free_all();
struct AST *copy_leaf(struct AST *leaf,bool deep);
char *mystrdup(char *data);
void print_lines();

void check_typedef(struct AST *type);

void show_error(int line,struct AST *element);
void show_debug(int line, struct AST *element, char *node);

#ifdef DO_PRINT
void print_tree(struct AST *,int,int,bool);
void do_print_tree(struct AST *);
#endif
