#!/usr/bin/env python3

import sys
import os
#sys.path.append(os.path.abspath(os.path.join(os.getcwd(), os.pardir)))

import pkg_resources
import unittest
from crust import crust

class Test(unittest.TestCase):

    do_print_debug = False
    do_print_debug2 = False
    test_list = []

    def _find_variable(self, var_container, var_name):
        for var_group in var_container["variables"]:
            if var_name in var_group:
                return var_group[var_name]
        return None

    def _find_variable_by_id(self, var_container, var_id):
        for var_group in var_container["variables"]:
            for var_name in var_group:
                if var_group[var_name]["uid"] == var_id:
                    return var_group[var_name]
        return None

    def _check_var_state(self, lib, variable, status, deep = 0):
        var = self._find_variable(lib.debug[deep], variable)
        self.assertIsNot(var, None)
        self.assertEqual(var["value"], status)

    def _check_block_id_is_the_same(self, lib, variable1, variable2, deep = 0):
        var1 = self._find_variable(lib.debug[deep], variable1)
        self.assertIsNot(var1, None)
        var2 = self._find_variable(lib.debug[deep], variable2)
        self.assertIsNot(var2, None)
        self.assertEqual(var1["uid"], var2["uid"])

    def _check_block_id_is_not_same(self, lib, variable1, variable2, deep = 0):
        var1 = self._find_variable(lib.debug[deep], variable1)
        self.assertIsNot(var1, None)
        var2 = self._find_variable(lib.debug[deep], variable2)
        self.assertIsNot(var2, None)
        self.assertNotEqual(var1["uid"], var2["uid"])

    def _test_msg(self, lib, error_element, params):
        with open("error_list.txt", "a") as errorlist:
            errorlist.write(error_element[1])
            errorlist.write("\n")
        self.assertEqual(len(error_element), len(params))
        for index in range(len(error_element)):
            self.assertEqual(error_element[index], params[index])

    def _print_errors(self, filename, lib):
        if self.do_print_debug2:
            print("\rtest file: {:s}".format(filename))
            lib.print_errors(False)

    def open_crust(self):
        if (os.path.exists('lexyacclib/crust_code.h')) and (os.path.exists('lexyacclib/crust.so')):
            lib = crust.crust("lexyacclib/crust_code.h", "lexyacclib/crust.so")
        else:
            lib = crust.crust(pkg_resources.resource_filename('crust','/crust_code.h'),pkg_resources.resource_filename('crust','/crust.so'))
        return lib

    def _generic_test(self, filename, params):
        if self.do_print_debug:
            print("Running test {:s}".format(filename))
        if filename in self.test_list:
            self.fail(f"Test file {filename} is repeated.")
        else:
            self.test_list.append(filename)
        lib = self.open_crust()
        tree = lib.read_file(filename, False, [], [], [])
        self.assertIsNot(tree, None)
        variables = lib.process_tree(tree, filename)
        lib.process_functions(tree, variables)
        self._print_errors(filename, lib)
        lib_errors = lib.get_errors_for_test()
        self.assertEqual(len(lib_errors), len(params))
        # avoid problems with error order
        lib_errors.sort()
        params.sort()
        for i in range(len(lib_errors)):
            self._test_msg(lib, lib_errors[i], params[i])
        return lib


    def _all_fine_test(self, filename):
        if self.do_print_debug:
            print("Running test {:s}".format(filename))
        if filename in self.test_list:
            self.fail(f"Test file {filename} is repeated.")
        else:
            self.test_list.append(filename)
        lib = self.open_crust()
        tree = lib.read_file(filename, False, [], [], [])
        self.assertIsNot(tree, None)
        variables = lib.process_tree(tree, filename)
        lib.process_functions(tree, variables)
        self._print_errors(filename, lib)
        lib_errors = lib.get_errors_for_test()
        self.assertEqual(len(lib_errors), 0)
        return lib

    def _raise_test(self, filename, exception_class, params):
        if self.do_print_debug:
            print("Running test {:s}".format(filename))
        if filename in self.test_list:
            self.fail(f"Test file {filename} is repeated.")
        else:
            self.test_list.append(filename)
        lib = self.open_crust()
        tree = lib.read_file(filename, False, [], [], [])
        self.assertIsNot(tree, None)
        variables = lib.process_tree(tree, filename)
        with self.assertRaises(exception_class):
            lib.process_functions(tree, variables)
        self._print_errors(filename, lib)
        lib_errors = lib.get_errors_for_test()
        self.assertEqual(len(lib_errors), len(params))
        for i in range(len(lib_errors)):
            self._test_msg(lib, lib_errors[i], params[i])
        return lib

    def test001AllFine(self):
        self._generic_test("unitest/test1.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 31) ])

    def test002UnknownVariable(self):
        self._raise_test("unitest/test2.c", crust.crust.VarNotFoundException, [(crust.crust.MSG_CRITICAL, "Unknown variable '{:s}' at line {:d}", "var", 4)])

    def test003Overwrite(self):
        self._generic_test("unitest/test3.c", [ (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", "param3", 7, 6) ])

    def test004UseUninitialized(self):
        self._generic_test("unitest/test4.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} isn't initialized", 1, "function", 7) ])

    def test005UseFreed(self):
        self._generic_test("unitest/test5.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "function", 6, 5) ])

    def test006PassedNonCrustyToCrusty(self):
        self._generic_test("unitest/test6.c", [ (crust.crust.MSG_CRITICAL, "Expected a __crust__ variable as argument {:d} when calling function '{:s}' at line {:d}, but passed non __crust__ variable", 1, "function", 8) ])

    def test007PassedCrustyToNonCrusty(self):
        self._generic_test("unitest/test7.c", [ (crust.crust.MSG_CRITICAL, "Expected a non __crust__ variable as argument {:d} when calling function '{:s}' at line {:d}, but passed a __crust__ variable", 1, "function", 8) ])

    def test008PassedOtherToCrusty(self):
        self._generic_test("unitest/test8.c", [ (crust.crust.MSG_CRITICAL, "Expected a __crust__ variable as argument {:d} when calling function '{:s}' at line {:d}", 1, "function", 7) ])

    def test009PassedIncorrectNumberOfParameters(self):
        self._generic_test("unitest/test9.c", [ (crust.crust.MSG_CRITICAL, "Calling function '{:s}' at line {:d} with an incorrect number of arguments", "function", 7) ])

    def test010NonExistingFunction(self):
        self._raise_test("unitest/test10.c", crust.crust.FunctionNotFoundException, [ (crust.crust.MSG_CRITICAL, "Calling function '{:s}' at line {:d}, but it is neither declared nor defined", "function3", 5) ])

    def test011CrustParameterNotPointer(self):
        self._generic_test("unitest/test11.c", [ (crust.crust.MSG_CRITICAL, "An argument is defined as __crust__ type at line {:d}, but it is not a pointer", 1) ])

    def test012CrustNamedParameterNotPointer(self):
        self._generic_test("unitest/test12.c", [ (crust.crust.MSG_CRITICAL, "Argument '{:s}' defined as __crust__ type at line {:d}, but it is not a pointer", "parameter", 1) ])

    def test013CrustVariableNotPointer(self):
        self._generic_test("unitest/test13.c", [ (crust.crust.MSG_CRITICAL, "Variable '{:s}' is defined as __crust__ type at line {:d}, but it is not a pointer", "retval", 5) ])

    def test014CrustRetvalNotPointer(self):
        self._generic_test("unitest/test14.c", [ (crust.crust.MSG_CRITICAL, "Return value for function '{:s}', defined at line {:d}, is defined as __crust__ type, but it is not a pointer", "function", 1) ])

    def test015CrustOnlyVarNoOthers(self):
        self._generic_test("unitest/test15.c", [ (crust.crust.MSG_CRITICAL, "Variable '{:s}', defined at line {:d}, have the __crust_recycle__ modifier, which is not allowed for defined variables", "var", 1) ])

    def test016RecycleOrBorrowNotBoth(self):
        self._generic_test("unitest/test16.c", [ (crust.crust.MSG_CRITICAL, "An argument can be BORROW or RECYCLE, not both. (error at line {:d})", 1) ])

    def test017NamedRecycleOrBorrowNotBoth(self):
        self._generic_test("unitest/test17.c", [ (crust.crust.MSG_CRITICAL, "An argument can be BORROW or RECYCLE, not both. (argument '{:s}' at line {:d})", "param", 1) ])

    def test018RecycleReturnsCrust(self):
        self._generic_test("unitest/test18.c", [ (crust.crust.MSG_CRITICAL, "The function '{:s}', at line {:d}, has a RECYCLE argument, but doesn't return a CRUST pointer", "function", 1) ])

    def test019OnlyOneRecycle(self):
        self._generic_test("unitest/test19.c", [ (crust.crust.MSG_CRITICAL, "The function '{:s}', at line {:d}, has more than one RECYCLE argument", "function", 1) ])

    def test020UninitializedVariable(self):
        self._generic_test("unitest/test20.c", [ (crust.crust.MSG_ERROR, "Using uninitialized variable '{:s}' at line {:d}", "param4", 5) ])

    def test021FreedVariable(self):
        self._all_fine_test("unitest/test21.c")
        #self._generic_test("unitest/test21.c", [ (crust.crust.MSG_ERROR, "Using variable '{:s}' at line {:d}, after being freed at line {:d}", "param4", 8, 7) ])

    def test022AssignFreedVariable(self):
        self._generic_test("unitest/test22.c", [ (crust.crust.MSG_ERROR, "Using variable '{:s}' at line {:d}, after being freed at line {:d}", "param1", 5, 4), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param4", 4, 7) ])

    def test023AssignUninitializedVariable(self):
        self._generic_test("unitest/test23.c", [ (crust.crust.MSG_ERROR, "Using uninitialized variable '{:s}' at line {:d}", "param4", 5) ])

    def test024AssignNonCrustToCrust(self):
        self._generic_test("unitest/test24.c", [ (crust.crust.MSG_CRITICAL, "Assigning the non-crust variable '{:s}' to the crust variable '{:s}' at line {:d}", "param4", "param5", 5) ])

    def test025AssignCrustToNonCrust(self):
        self._generic_test("unitest/test25.c", [ (crust.crust.MSG_CRITICAL, "Assigning the crust variable '{:s}' to the non-crust variable '{:s}' at line {:d}", "param4", "param5", 5) ])

    def test026TypeCast(self):
        self._generic_test("unitest/test26.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 6), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param", 6, 8) ])

    def test027AssignNonCrustToCrust2(self):
        self._generic_test("unitest/test27.c", [ (crust.crust.MSG_CRITICAL, "Assigning a non-crust value to the crust variable '{:s}' at line {:d}", "param", 5), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param", 5, 7) ])

    def test028CrustRetVal(self):
        self._generic_test("unitest/test28.c", [ (crust.crust.MSG_ERROR, "Calling the function '{:s}' at line {:d}, but ignoring the crust-type return value", "function", 7) ])

    def test029UseUninitialized2(self):
        self._generic_test("unitest/test29.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} isn't initialized", 1, "function", 7) ])

    def test030UseFreed2(self):
        self._generic_test("unitest/test30.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "function", 6, 5) ])

    def test031PassedNonCrustyToCrusty2(self):
        self._generic_test("unitest/test31.c", [ (crust.crust.MSG_CRITICAL, "Expected a __crust__ variable as argument {:d} when calling function '{:s}' at line {:d}, but passed non __crust__ variable", 1, "function", 8) ])

    def test032PassedCrustyToNonCrusty2(self):
        self._generic_test("unitest/test32.c", [ (crust.crust.MSG_CRITICAL, "Expected a non __crust__ variable as argument {:d} when calling function '{:s}' at line {:d}, but passed a __crust__ variable", 1, "function", 6) ])

    def test033PassedOtherToCrusty2(self):
        self._generic_test("unitest/test33.c", [ (crust.crust.MSG_CRITICAL, "Expected a __crust__ variable as argument {:d} when calling function '{:s}' at line {:d}", 1, "function", 6) ])

    def test034MultiAssign(self):
        self._generic_test("unitest/test34.c", [ (crust.crust.MSG_CRITICAL, "Assigning a non-crust value to the crust variable '{:s}' at line {:d}", "b", 6), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "a", 6, 8)] )

    def test035AmpPointer(self):
        self._all_fine_test("unitest/test35.c")

    def test036StructElement(self):
        self._all_fine_test("unitest/test36.c")

    def test037StructAssign(self):
        self._generic_test("unitest/test37.c", [ (crust.crust.MSG_CRITICAL, "Assigning the non-crust variable '{:s}' to the crust variable '{:s}' at line {:d}", "test", "param4", 10) ])

    def test038ArrayElement(self):
        self._all_fine_test("unitest/test38.c")

    def test039ArrayAssign(self):
        self._generic_test("unitest/test39.c", [ (crust.crust.MSG_CRITICAL, "Assigning the non-crust variable '{:s}' to the crust variable '{:s}' at line {:d}", "test", "param4", 6) ])

    def test040StructAssignment(self):
        self._all_fine_test("unitest/test40.c")

    def test041UsePostStructAssignment(self):
        self._generic_test("unitest/test41.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "function", 9, 8) ])

    def test042UsePostStructAssignment2(self):
        self._generic_test("unitest/test42.c", [ (crust.crust.MSG_CRITICAL, "Assigning the crust variable '{:s}' to the non-crust variable '{:s}' at line {:d}", 'param3', "test", 8) ])

    def test043StructAssignmentByRef(self):
        self._all_fine_test("unitest/test43.c")

    def test044IfEqNull(self):
        self._generic_test("unitest/test44.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 7), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param", 5, 14), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param", 10, 14), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "var2", 7, 14) ] )


    def test045IfNoEqNull(self):
        self._generic_test("unitest/test45.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 7)])

    def test046IfElseNull(self):
        self._generic_test("unitest/test46.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 7), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "var2", 7, 14) ])

    def test047Recycle(self):
        lib = self._generic_test("unitest/test47.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 10)])
        self._check_var_state(lib, "var3", crust.crust.VALUE_NOT_NULL, 0)
        self._check_var_state(lib, "var3", crust.crust.VALUE_FREED, 1)

    def test048Borrow(self):
        lib = self._generic_test("unitest/test48.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 11)])
        self._check_var_state(lib, "var2", crust.crust.VALUE_NOT_NULL)

    def test049Borrow2(self):
        lib = self._all_fine_test("unitest/test49.c")
        self._check_var_state(lib, "var2", crust.crust.VALUE_NULL)

    def test050PassBorrowToNonBorrow(self):
        self._generic_test("unitest/test50.c", [ (crust.crust.MSG_CRITICAL, "Argument '{:s}' at position {:d} when calling function '{:s}' at line {:d} is borrowed, but is used as a non-borrow argument", 'param', 1, "test_function", 8) ])

    def test051PassBorrowToBorrow(self):
        self._all_fine_test("unitest/test51.c")

    def test052PassBorrowToBorrow2(self):
        self._all_fine_test("unitest/test52.c")

    def test053AssignFunctionToFunctionPointerLocal(self):
        self._all_fine_test("unitest/test53.c")

    def test054AssignFunctionToFunctionPointerGlobal(self):
        self._all_fine_test("unitest/test54.c")

    def test055AssignFunctionPointerToFunctionName(self):
        self._generic_test("unitest/test55.c", [ (crust.crust.MSG_CRITICAL, "Trying to assign a value to the function name '{:s}' at line {:d}", 'test_function', 10) ])

    def test056ComparisonDoesntFreePointer(self):
        self._generic_test("unitest/test56.c", [ (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", "param1", 5, 3) ])

    def test057ComparisonWithoutBraces(self):
        self._all_fine_test("unitest/test57.c")

    def test058NestedComparisonWithoutBraces(self):
        self._all_fine_test("unitest/test58.c")

    def test059NestedComparisonWithBraces(self):
        self._all_fine_test("unitest/test59.c")

    def test060IfWithNot(self):
        self._generic_test("unitest/test60.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 7)])

    def test061IfWithNot2(self):
        self._generic_test("unitest/test61.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 9), (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", "param", 11, 7) ])

    def test062Return(self):
        self._all_fine_test("unitest/test62.c")

    def test063BlockInUseAtEnd(self):
        self._generic_test("unitest/test63.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param", 5, 8) ])

    def test064BlockInUseAtEnd2(self):
        self._generic_test("unitest/test64.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param", 5, 7) ])

    def test065BlockInUseWithReturn(self):
        self._generic_test("unitest/test65.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "var2", 8, 9) ])

    def test066BlockInUseWithReturn2(self):
        self._generic_test("unitest/test66.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "var2", 9, 12) ])

    def test067BlockInUseWithReturn3(self):
        self._all_fine_test("unitest/test67.c")

    def test068AssignmentToBorrowBlock(self):
        self._generic_test("unitest/test68.c", [(crust.crust.MSG_ERROR, "Overwritting the borrowed argument '{:s}' at line {:d}", "param", 7) ])

    def test069AssignmentFromBorrowBlock(self):
        self._generic_test("unitest/test69.c", [ (crust.crust.MSG_CRITICAL, "Assigning a borrowed block into a local variable is not allowed (assigning '{:s}' at line {:d})", "param", 9) ])

    def test070AssignmentFromBorrowBlock2(self):
        self._generic_test("unitest/test70.c", [ (crust.crust.MSG_CRITICAL, "Assigning a borrowed block into a local variable is not allowed (assigning '{:s}' at line {:d})", "param", 9) ])

    def test071AssignmentToAndFromMemberOfBorrowBlock(self):
        self._generic_test("unitest/test71.c", [ (crust.crust.MSG_WARNING, "Using variable '{:s}' at line {:d} with a possible NULL value", "param", 10) ])

    def test072AliasFreesBlock(self):
        lib = self._all_fine_test("unitest/test72.c")
        self._check_var_state(lib, "param", crust.crust.VALUE_FREED_OR_NULL)
        self._check_var_state(lib, "an_alias", crust.crust.VALUE_FREED_OR_NULL)

    def test073AliasFreesBlock2(self):
        self._generic_test("unitest/test73.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "main", 9, 8) ])

    def test074BlockFreesAlias(self):
        self._generic_test("unitest/test74.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "main", 10, 9) ])

    def test075DecoupleAlias(self):
        lib = self._generic_test("unitest/test75.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "main", 13, 9) ])
        self._check_var_state(lib, "an_alias", crust.crust.VALUE_NOT_NULL_OR_NULL, 0)
        self._check_var_state(lib, "an_alias", crust.crust.VALUE_FREED_OR_NULL, 1)
        self._check_var_state(lib, "an_alias", crust.crust.VALUE_NULL, 2)


    def test076DecoupleAlias2(self):
        lib = self._all_fine_test("unitest/test76.c")
        self._check_var_state(lib, "param1", crust.crust.VALUE_FREED_OR_NULL, 0)
        self._check_var_state(lib, "param2", crust.crust.VALUE_NOT_NULL_OR_NULL, 0)
        self._check_var_state(lib, "an_alias", crust.crust.VALUE_NOT_NULL_OR_NULL, 0)
        self._check_var_state(lib, "param2", crust.crust.VALUE_FREED_OR_NULL, 1)
        self._check_var_state(lib, "an_alias", crust.crust.VALUE_FREED_OR_NULL, 1)

    def test077DecoupleAlias3(self):
        lib = self._generic_test("unitest/test77.c", [ (crust.crust.MSG_ERROR, "Assigning the crust-type result value of function '{:s}' to the alias '{:s}' at line {:d}", "function", "an_alias", 11) ])
        self._check_var_state(lib, "param", crust.crust.VALUE_FREED_OR_NULL, 1)
        self._check_var_state(lib, "an_alias", crust.crust.VALUE_NOT_NULL_OR_NULL, 1)

    def test078FunctionPointerNull(self):
        self._generic_test("unitest/test78.c", [ (crust.crust.MSG_ERROR, "Using function pointer '{:s}' at line {:d} with NULL value", "test1", 9) ])

    def test079FunctionPointerNull2(self):
        self._generic_test("unitest/test79.c", [ (crust.crust.MSG_ERROR, "Using function pointer '{:s}' at line {:d} with NULL value", "test1", 7) ])

    def test080FunctionPointerFine(self):
        self._all_fine_test("unitest/test80.c")


    def test081FunctionPointerFine2(self):
        self._all_fine_test("unitest/test81.c")

    def test082FunctionPointerNonInitialized(self):
        self._generic_test("unitest/test82.c", [ (crust.crust.MSG_ERROR, "Using uninitialized function pointer '{:s}' at line {:d}", "test1", 9) ])

    def test083FunctionPointerNonInitialized2(self):
        self._generic_test("unitest/test83.c", [ (crust.crust.MSG_ERROR, "Using uninitialized function pointer '{:s}' at line {:d}", "test1", 7) ])

    def test084UndefinedPointerFunction(self):
        self._raise_test("unitest/test84.c", crust.crust.FunctionNotFoundException, [ (crust.crust.MSG_CRITICAL, "Calling function '{:s}' at line {:d}, but it is neither declared nor defined", "test1", 6) ])

    def test085FunctionPointerIncorrectNumberOfParameters(self):
        self._generic_test("unitest/test85.c", [ (crust.crust.MSG_CRITICAL, "Calling function '{:s}' at line {:d} with an incorrect number of arguments", "test1", 11) ])

    def test086FunctionPointerIncorrectNumberOfParameters2(self):
        self._generic_test("unitest/test86.c", [ (crust.crust.MSG_CRITICAL, "Calling function '{:s}' at line {:d} with an incorrect number of arguments", "test1", 9) ])

    def test087FunctionPointerExpectedCrustParameter(self):
        self._generic_test("unitest/test87.c", [ (crust.crust.MSG_CRITICAL, "Expected a __crust__ variable as argument {:d} when calling function '{:s}' at line {:d}, but passed non __crust__ variable", 1, "test1", 12) ])

    def test088FunctionPointerExpectedCrustParameter2(self):
        self._generic_test("unitest/test88.c", [ (crust.crust.MSG_CRITICAL, "Expected a __crust__ variable as argument {:d} when calling function '{:s}' at line {:d}, but passed non __crust__ variable", 1, "test1", 10) ])

    def test089FunctionPointerExpectedNonCrustParameter(self):
        self._generic_test("unitest/test89.c", [ (crust.crust.MSG_CRITICAL, "Expected a non __crust__ variable as argument {:d} when calling function '{:s}' at line {:d}, but passed a __crust__ variable", 1, "test1", 11), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param1", 9 , 12) ])

    def test090FunctionPointerExpectedNonCrustParameter2(self):
        self._generic_test("unitest/test90.c", [ (crust.crust.MSG_CRITICAL, "Expected a non __crust__ variable as argument {:d} when calling function '{:s}' at line {:d}, but passed a __crust__ variable", 1, "test1", 9), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param1", 7 , 10) ])

    def test091NestedVariables(self):
        lib = self._all_fine_test("unitest/test91.c")
        self._check_var_state(lib, "tmp1", crust.crust.VALUE_UNINITIALIZED, 1)
        self._check_var_state(lib, "tmp1", crust.crust.VALUE_NULL, 0)

    def test092FunctionPointerFineNoParams(self):
        self._all_fine_test("unitest/test92.c")

    def test093FunctionPointerFineNoParams2(self):
        self._all_fine_test("unitest/test93.c")

    def test094FunctionPointerReturnsCrust(self):
        self._generic_test("unitest/test94.c", [ (crust.crust.MSG_ERROR, "Calling the function '{:s}' at line {:d}, but ignoring the crust-type return value", "test1", 11) ])

    def test095FunctionPointerReturnsCrust2(self):
        self._generic_test("unitest/test95.c", [ (crust.crust.MSG_ERROR, "Calling the function '{:s}' at line {:d}, but ignoring the crust-type return value", "test1", 9) ])

    def test096BreakOutsideLoop(self):
        self._generic_test("unitest/test96.c", [ (crust.crust.MSG_CRITICAL, "Break not in a loop at line {:d}", 2) ])

    def test097WhileFreesTwice(self):
        self._generic_test("unitest/test97.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "main", 10, 10) ])

    def test098AndOpFine(self):
        self._generic_test("unitest/test98.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param1", 7 , 13), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param2", 7 , 13), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param1", 10 , 13), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param2", 11 , 13) ])


    def test099OrOpFine(self):
        self._generic_test("unitest/test99.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param1", 7 , 13), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param2", 7 , 13), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param1", 10 , 13), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param2", 11 , 13) ])

    def test100AndOpWrong(self):
        self._generic_test("unitest/test100.c", [ (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", "param1", 10, 7), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param1", 7 , 13), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param2", 7 , 13), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param1", 10 , 13), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param2", 11 , 13) ])

    def test101OrOpWrong(self):
        self._generic_test("unitest/test101.c", [ (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", "param2", 11, 7), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param1", 7 , 13), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param2", 7 , 13), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param1", 10 , 13), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param2", 11 , 13) ])

    def test102WhileCrustVarFine(self):
        self._generic_test("unitest/test102.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 8) ])

    def test103WhileWithoutNoZero(self):
        self._generic_test("unitest/test103.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} isn't initialized", 1, "function", 15), (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 12) ])

    def test104WhileWithNoZeroFine(self):
        self._generic_test("unitest/test104.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 12) ])

    def test105DifferenParameterNumberNoCrust(self):
        self._all_fine_test("unitest/test105.c")

    def test106DifferenParameterNumberCrust(self):
        self._generic_test("unitest/test106.c", [ (crust.crust.MSG_ERROR, "Trying to assign the function '{:s}' to the function pointer '{:s}' at line {:d}, but they have a different number of arguments", "function", "test1", 10) ])

    def test107DifferenParameterNumberCrust(self):
        self._generic_test("unitest/test107.c", [ (crust.crust.MSG_ERROR, "Trying to assign the function '{:s}' to the function pointer '{:s}' at line {:d}, but they have a different number of arguments", "function", "test1", 10) ])

    def test108DifferenParameterTypes(self):
        self._generic_test("unitest/test108.c", [ (crust.crust.MSG_ERROR, "Trying to assign the function '{:s}' to the function pointer '{:s}' at line {:d}, but argument {:d} differs", "function", "test1", 10, 1) ])

    def test109DifferenParameterTypes(self):
        self._generic_test("unitest/test109.c", [ (crust.crust.MSG_ERROR, "Trying to assign the function '{:s}' to the function pointer '{:s}' at line {:d}, but the return types differ", "function", "test1", 10) ])

    def test110ParameterTypesMatch(self):
        self._all_fine_test("unitest/test110.c")

    def test111CommaOperatorFine(self):
        self._generic_test("unitest/test111.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param", 12 , 13) ])

    def test112CommaOperatorBad(self):
        self._generic_test("unitest/test112.c", [ (crust.crust.MSG_ERROR, "Calling the function '{:s}' at line {:d}, but ignoring the crust-type return value", "function", 12), (crust.crust.MSG_CRITICAL, "Assigning the non-crust variable '{:s}' to the crust variable '{:s}' at line {:d}", "b", "param", 12), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param", 12 , 13) ])

    def test113CommaOperatorFine2(self):
        self._generic_test("unitest/test113.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param", 12 , 13) ])

    def test114CheckSelfAssignOps(self):
        self._all_fine_test("unitest/test114.c")

    def test115VariableNumberOfArguments(self):
        self._generic_test("unitest/test115.c", [ (crust.crust.MSG_CRITICAL, "Passed a __crust__ variable as argument {:d} when calling function '{:s}' at line {:d}, but __crust__ variables are not allowed for optional arguments in functions with variable number of arguments", 3, "function", 11), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param", 7 , 12) ])

    def test116NonCrustRetValToCrust(self):
        self._generic_test("unitest/test116.c", [ (crust.crust.MSG_ERROR, "Assigning the non-crust-type result value of function '{:s}' at line {:d} to the crust variable '{:s}'", "function", 5, "retval"), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "retval", 5 , 6) ])

    def test117CrustRetValToNonCrust(self):
        self._generic_test("unitest/test117.c", [ (crust.crust.MSG_ERROR, "Assigning the crust-type result value of function '{:s}' at line {:d} to the non-crust variable '{:s}'", "function", 5, "retval") ])

    def test118CallingPointerWithPosibleNull(self):
        self._generic_test("unitest/test118.c", [ (crust.crust.MSG_WARNING, "Using function pointer '{:s}' at line {:d} with a possible NULL value", "test1", 11) ])

    def test119BlockToGlobalVar(self):
        self._all_fine_test("unitest/test119.c")

    def test120DoWhileUsesTwice(self):
        self._generic_test("unitest/test120.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "main", 10, 10) ])

    def test121DoWhileExitsFine(self):
        self._generic_test("unitest/test121.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 9) ])

    def test122DoWhileExitsBreak(self):
        self._generic_test("unitest/test122.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 12) ])

    def test123ForDefinedOutside(self):
        self._generic_test("unitest/test123.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "main", 10, 10) ])

    def test124ForDefinedInside(self):
        self._generic_test("unitest/test124.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "main", 8, 8) ])

    def test125ForDefinedOutside(self):
        self._generic_test("unitest/test125.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "main", 10, 10) ])

    def test126ForFreesBlocks(self):
        self._all_fine_test("unitest/test126.c")

    def test127ForProcessesBlocks(self):
        self._generic_test("unitest/test127.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param", 7, 13) ])

    def test128AssignAliasFromAlias(self):
        lib = self._generic_test("unitest/test128.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param", 5, 15) ])
        self._check_var_state(lib, "param", crust.crust.VALUE_NOT_NULL_OR_NULL, 0)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_UNINITIALIZED, 0)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_UNINITIALIZED, 0)


        self._check_var_state(lib, "param", crust.crust.VALUE_NOT_NULL_OR_NULL, 1)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_NOT_NULL_OR_NULL, 1)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_UNINITIALIZED, 1)

        self._check_var_state(lib, "param", crust.crust.VALUE_NOT_NULL_OR_NULL, 2)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_NOT_NULL_OR_NULL, 2)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_NOT_NULL_OR_NULL, 2)

    def test129AssignAliasFromAlias2(self):
        lib = self._all_fine_test("unitest/test129.c")
        self._check_var_state(lib, "param1", crust.crust.VALUE_NOT_NULL_OR_NULL, 0)
        self._check_var_state(lib, "param2", crust.crust.VALUE_NOT_NULL, 0)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_UNINITIALIZED, 0)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_UNINITIALIZED, 0)
        self._check_block_id_is_not_same(lib, "param1", "param2", 0)

        self._check_var_state(lib, "param1", crust.crust.VALUE_NOT_NULL_OR_NULL, 1)
        self._check_var_state(lib, "param2", crust.crust.VALUE_NOT_NULL, 1)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_NOT_NULL_OR_NULL, 1)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_UNINITIALIZED, 1)
        self._check_block_id_is_not_same(lib, "param1", "param2", 1)
        self._check_block_id_is_the_same(lib, "param1", "alias1", 1)

        self._check_var_state(lib, "param1", crust.crust.VALUE_NOT_NULL_OR_NULL, 2)
        self._check_var_state(lib, "param2", crust.crust.VALUE_NOT_NULL, 2)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_NOT_NULL_OR_NULL, 2)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_NOT_NULL_OR_NULL, 2)
        self._check_block_id_is_not_same(lib, "param1", "param2", 2)
        self._check_block_id_is_the_same(lib, "param1", "alias1", 2)
        self._check_block_id_is_the_same(lib, "param1", "alias2", 2)

        self._check_var_state(lib, "param1", crust.crust.VALUE_NOT_NULL_OR_NULL, 3)
        self._check_var_state(lib, "param2", crust.crust.VALUE_NOT_NULL, 3)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_NOT_NULL, 3)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_NOT_NULL_OR_NULL, 3)
        self._check_block_id_is_not_same(lib, "param1", "param2", 3)
        self._check_block_id_is_the_same(lib, "param2", "alias1", 3)
        self._check_block_id_is_the_same(lib, "param1", "alias2", 3)

        self._check_var_state(lib, "param1", crust.crust.VALUE_FREED_OR_NULL, 4)
        self._check_var_state(lib, "param2", crust.crust.VALUE_FREED, 4)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_FREED, 4)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_FREED_OR_NULL, 4)


    def test130AssignAliasFromAlias(self):
        lib = self._all_fine_test("unitest/test130.c")
        self._check_var_state(lib, "param", crust.crust.VALUE_NOT_NULL_OR_NULL, 0)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_UNINITIALIZED, 0)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_UNINITIALIZED, 0)

        self._check_var_state(lib, "param", crust.crust.VALUE_NOT_NULL_OR_NULL, 1)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_NOT_NULL_OR_NULL, 1)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_UNINITIALIZED, 1)
        self._check_block_id_is_the_same(lib, "param", "alias1", 1)

        self._check_var_state(lib, "param", crust.crust.VALUE_NOT_NULL_OR_NULL, 2)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_NOT_NULL_OR_NULL, 2)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_NOT_NULL_OR_NULL, 2)
        self._check_block_id_is_the_same(lib, "param", "alias1", 2)
        self._check_block_id_is_the_same(lib, "param", "alias2", 2)

        self._check_var_state(lib, "param", crust.crust.VALUE_NOT_NULL_OR_NULL, 3)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_NOT_NULL_OR_NULL, 3)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_NOT_NULL_OR_NULL, 3)
        self._check_block_id_is_not_same(lib, "param", "alias1", 3)
        self._check_block_id_is_the_same(lib, "param", "alias2", 3)

        self._check_var_state(lib, "param", crust.crust.VALUE_NOT_NULL, 4)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_FREED_OR_NULL, 4)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_NOT_NULL, 4)
        self._check_block_id_is_not_same(lib, "param", "alias1", 4)
        self._check_block_id_is_the_same(lib, "param", "alias2", 4)


    def test131AssignAliasFromAlias(self):
        lib = self._all_fine_test("unitest/test131.c")
        self._check_var_state(lib, "param", crust.crust.VALUE_NOT_NULL_OR_NULL, 0)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_UNINITIALIZED, 0)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_UNINITIALIZED, 0)

        self._check_var_state(lib, "param", crust.crust.VALUE_NOT_NULL_OR_NULL, 1)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_NOT_NULL_OR_NULL, 1)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_UNINITIALIZED, 1)
        self._check_block_id_is_the_same(lib, "param", "alias1", 1)

        self._check_var_state(lib, "param", crust.crust.VALUE_NOT_NULL_OR_NULL, 2)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_NOT_NULL_OR_NULL, 2)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_NOT_NULL_OR_NULL, 2)
        self._check_block_id_is_the_same(lib, "param", "alias1", 2)
        self._check_block_id_is_the_same(lib, "param", "alias2", 2)

        self._check_var_state(lib, "param", crust.crust.VALUE_FREED, 3)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_FREED, 3)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_FREED, 3)

    def test132AssignAliasFromAlias(self):
        lib = self._all_fine_test("unitest/test132.c")
        self._check_var_state(lib, "param1", crust.crust.VALUE_NOT_NULL_OR_NULL, 0)
        self._check_var_state(lib, "param2", crust.crust.VALUE_NOT_NULL, 0)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_UNINITIALIZED, 0)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_UNINITIALIZED, 0)
        self._check_block_id_is_not_same(lib, "param1", "param2", 0)

        self._check_var_state(lib, "param1", crust.crust.VALUE_NOT_NULL_OR_NULL, 1)
        self._check_var_state(lib, "param2", crust.crust.VALUE_NOT_NULL, 1)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_NOT_NULL_OR_NULL, 1)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_UNINITIALIZED, 1)
        self._check_block_id_is_not_same(lib, "param1", "param2", 1)
        self._check_block_id_is_the_same(lib, "param1", "alias1", 1)

        self._check_var_state(lib, "param1", crust.crust.VALUE_NOT_NULL_OR_NULL, 2)
        self._check_var_state(lib, "param2", crust.crust.VALUE_NOT_NULL, 2)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_NOT_NULL_OR_NULL, 2)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_NOT_NULL, 2)
        self._check_block_id_is_not_same(lib, "param1", "param2", 2)
        self._check_block_id_is_the_same(lib, "param1", "alias1", 2)
        self._check_block_id_is_the_same(lib, "param2", "alias2", 2)

        self._check_var_state(lib, "param1", crust.crust.VALUE_FREED_OR_NULL, 3)
        self._check_var_state(lib, "param2", crust.crust.VALUE_NOT_NULL, 3)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_FREED_OR_NULL, 3)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_NOT_NULL, 3)
        self._check_block_id_is_the_same(lib, "param2", "alias2", 3)

        self._check_var_state(lib, "param1", crust.crust.VALUE_FREED_OR_NULL, 4)
        self._check_var_state(lib, "param2", crust.crust.VALUE_FREED, 4)
        self._check_var_state(lib, "alias1", crust.crust.VALUE_FREED_OR_NULL, 4)
        self._check_var_state(lib, "alias2", crust.crust.VALUE_FREED, 4)


    def test133GotoJumpsOverAll(self):
        self._generic_test("unitest/test133.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param", 12, 21) ])

    def test134ReturnCrustExpectCrust(self):
        self._all_fine_test("unitest/test134.c")

    def test135ReturnNoCrustExpectNoCrust(self):
        self._all_fine_test("unitest/test135.c")

    def test136ReturnCrustExpectNoCrust(self):
        self._generic_test("unitest/test136.c", [ (crust.crust.MSG_ERROR, "Return statement at line {:d} is returning a crust value, but is must return a non-crust one", 7) ])

    def test137ReturnNoCrustExpectCrust(self):
        self._generic_test("unitest/test137.c", [ (crust.crust.MSG_ERROR, "Return statement at line {:d} is returning a non-crust value, but is must return a crust one", 7) ])

    def test138OddBreakInFor(self):
        self._generic_test("unitest/test138.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param1", 7, 15) ])

    def test139BreakInNestedFor(self):
        self._generic_test("unitest/test139.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param2", 7, 22), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param1", 7, 22) ])

    def test140Switch1(self):
        self._generic_test("unitest/test140.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param1", 7, 27), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param2", 7, 27), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param3", 7, 27) ])

    def test141Switch2(self):
        self._generic_test("unitest/test141.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "function", 23, 11), (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "function", 23, 14), (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "function", 23, 17), (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "function", 23, 20) ])

    def test142Switc3(self):
        self._generic_test("unitest/test142.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "function", 15, 13), (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "function", 15, 11), (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "function", 13, 11) ])

    def test143CheckSignatures(self):
        self._generic_test("unitest/test143.c", [ (crust.crust.MSG_CRITICAL, "Function definition for '{:s}' at line {:d}, file '{:s}' differs from definition at line {:d}, file '{:s}'", "main", 7, "unitest/test143.c", 5, "unitest/test143.c") ])

    def test144CheckSignatures(self):
        self._generic_test("unitest/test144.c", [ (crust.crust.MSG_CRITICAL, "Function definition for '{:s}' at line {:d}, file '{:s}' differs from definition at line {:d}, file '{:s}'", "main", 7, "unitest/test144.c", 5, "unitest/test144.c") ])

    def test145CheckAssignAndCallPointerToPossibleNull(self):
        self._generic_test("unitest/test145.c", [ (crust.crust.MSG_WARNING, "Using variable '{:s}' at line {:d} with a possible NULL value", "param1", 16), (crust.crust.MSG_WARNING, "Using variable '{:s}' at line {:d} with a possible NULL value", "param1", 17) ])

    def test146CheckAssignToElementOfNullPointer(self):
        self._generic_test("unitest/test146.c", [ (crust.crust.MSG_ERROR, "Using variable '{:s}' at line {:d}, but it has been already freed", "param1", 15) ])

    def test147CheckAssignToElementOfUninitializedPointer(self):
        self._generic_test("unitest/test147.c", [ (crust.crust.MSG_ERROR, "Using variable '{:s}' at line {:d}, but it hasn't been initialized yet", "param1", 15) ])

    def test148CheckStructAccessWithoutPointer(self):
        self._all_fine_test("unitest/test148.c")

    def test149TestWhileContinue(self):
        self._generic_test("unitest/test149.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 12), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param", 12, 16), (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", "param", 12, 12) ])

    def test150TestDoWhileContinue(self):
        self._generic_test("unitest/test150.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 12), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param", 12, 16), (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", "param", 12, 12) ])

    def test151TestForContinue(self):
        self._generic_test("unitest/test151.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 12), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param", 12, 16), (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", "param", 12, 12) ])

    def test152TestWhileContinue2(self):
        self._generic_test("unitest/test152.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} isn't initialized", 1, "function", 16), (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 12), (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", "param", 12, 12) ])

    def test153TestDoWhileContinue2(self):
        self._generic_test("unitest/test153.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 12), (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", "param", 12, 12) ])

    def test154TestForContinue2(self):
        self._generic_test("unitest/test154.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} isn't initialized", 1, "function", 16), (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 12), (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", "param", 12, 12) ])

    def test155TestWhileContinue3(self):
        self._generic_test("unitest/test155.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 12), (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", "param", 12, 12) ])

    def test156TestForContinue3(self):
        self._generic_test("unitest/test156.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 12), (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", "param", 12, 12) ])

    def test157CheckEnums(self):
        self._all_fine_test("unitest/test157.c")

    def test158TestBorrowVariables(self):
        self._generic_test("unitest/test158.c", [ (crust.crust.MSG_CRITICAL, "Global variable '{:s}', defined as __crust_borrow__ in line {:d}, but that is not allowed", "global_var", 1) ])

    def test159TestBorrowVariables2(self):
        self._generic_test("unitest/test159.c", [ (crust.crust.MSG_ERROR, "Assigning the non-borrowed block '{:s}' to the borrowed variable '{:s}' at line {:d}", "param2", "var2", 12), (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 14), (crust.crust.MSG_ERROR, "Assigning a value to the borrowed variable '{:s}' at line {:d}", "var4", 14) ])

    def test160AssignRetBorrowToNonBorrow(self):
        self._generic_test("unitest/test160.c", [ (crust.crust.MSG_ERROR, "Assigning the borrowed result value of function '{:s}' to the non-borrowed variable '{:s}' at line {:d}", "function1", "var1", 11), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "var1", 11, 12) ])

    def test161AssignRetNonBorrowToNonBorrow(self):
        self._generic_test("unitest/test161.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "var1", 11, 12) ])

    def test162AssignRetBorrowToBorrow(self):
        self._all_fine_test("unitest/test162.c")

    def test163AssignRetBorrowToNonBorrow(self):
        self._generic_test("unitest/test163.c", [ (crust.crust.MSG_ERROR, "Assigning the non-borrowed result value of function '{:s}' to the borrowed variable '{:s}' at line {:d}", "function1", "var1", 11) ])

    def test164AssignRetBorrowToBorrowParam(self):
        self._generic_test("unitest/test164.c", [ (crust.crust.MSG_ERROR, "Overwritting the borrowed argument '{:s}' at line {:d}", "param1", 9) ])

    def test165ExpectedReturnValueButNoValueAtEnd(self):
        self._generic_test("unitest/test165.c", [ (crust.crust.MSG_ERROR, "Function '{:s}' expects a return value, but the code exited without it", "main") ])

    def test166ExpectedNoReturnValueButValueAvailable(self):
        self._generic_test("unitest/test166.c", [ (crust.crust.MSG_ERROR, "Return statement at line {:d} is returning a value, but the function '{:s}' does not return a value", 7, "main") ])

    def test167ExpectedReturnValueEmptyReturn(self):
        self._generic_test("unitest/test167.c", [ (crust.crust.MSG_ERROR, "Function '{:s}' expects a return value, but the code exited at line {:d} without it", "main", 9) ])

    def test168ExpectedReturnValueEmptyReturn(self):
        self._generic_test("unitest/test168.c", [ (crust.crust.MSG_ERROR, "Function '{:s}' expects a borrowed value to return, but a non-borrowed was used at line {:d}", "function1", 6) ])

    def test169ReturnedFreedValue(self):
        self._generic_test("unitest/test169.c", [ (crust.crust.MSG_ERROR, "Returning variable '{:s}' at line {:d} was freed at line {:d}", "param", 7, 6) ])

    def test170ReturnedUninitializedValue(self):
        self._generic_test("unitest/test170.c", [ (crust.crust.MSG_ERROR, "Returning variable '{:s}' at line {:d} is uninitialized", "param", 5) ])

    def test171IgnoreBorrowReturnValue(self):
        self._all_fine_test("unitest/test171.c")

    def test172AssignCrustToNoCrustThroughTypecast(self):
        self._generic_test("unitest/test172.c", [ (crust.crust.MSG_WARNING, "Typecasting a CRUST value into a non-CRUST type at line {:d}", 11), (crust.crust.MSG_WARNING, "Assigning, with a typecast, the crust-type result value of function '{:s}' at line {:d} to the non-crust variable '{:s}'", "function1", 11, "var1") ])

    def test173TestNoNullParameter(self):
        self._all_fine_test("unitest/test173.c")

    def test174AssignCrustToNoCrustThroughTypecast(self):
        self._generic_test("unitest/test174.c", [ (crust.crust.MSG_WARNING, "Argument '{:s}' at position {:d} when calling function '{:s}' at line {:d} is defined as not_null, but is being called with a possible NULL value", "param", 1, "function1", 9) ])

    def test175AssignCrustToNoCrustThroughTypecast(self):
        self._generic_test("unitest/test175.c", [ (crust.crust.MSG_ERROR, "Passing NULL as argument {:d} when calling function '{:s}' at line {:d}, but it must be a not_null value", 1, "function1", 9) ])

    def test176AssignCrustToNoCrustThroughTypecast(self):
        self._generic_test("unitest/test176.c", [ (crust.crust.MSG_ERROR, "Argument '{:s}' at position {:d} when calling function '{:s}' at line {:d} is defined as not_null, but is being called with a NULL value", "param", 1, "function1", 10) ])

    def test177TestNoNullParameter(self):
        self._all_fine_test("unitest/test177.c")

    def test178AssignCrustToNoCrustThroughTypecast(self):
        self._generic_test("unitest/test178.c", [ (crust.crust.MSG_ERROR, "Return value at line {:d} is NULL, but the function must not return a NULL value", 7) ])

    def test179AssignCrustToNoCrustThroughTypecast(self):
        self._generic_test("unitest/test179.c", [ (crust.crust.MSG_WARNING, "Return value at line {:d} is a possible NULL, but the function must not return a NULL value", 7) ])

    def test180ReturnFunctionReturn(self):
        self._all_fine_test("unitest/test180.c")

    def test181CheckIfWithUninitializedVariable(self):
        self._generic_test("unitest/test181.c", [ (crust.crust.MSG_ERROR, "Using uninitialized variable '{:s}' at line {:d}", "param1", 7) ])

    def test182CheckCompareWithNullWhenFreed(self):
        self._all_fine_test("unitest/test182.c")

    def test183CheckIfWithUninitializedVariable(self):
        self._generic_test("unitest/test183.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param3", 5, 9) ])

    def test184CheckAssignToSeveralGlobalVars(self):
        lib = self._generic_test("unitest/test184.c", [ (crust.crust.MSG_ERROR, "At exit point in line {:d}, global variable '{:s}' points to the same block than global variable '{:s}'.", 17, "global_var1", "global_var2") ])
        self._check_block_id_is_the_same(lib, "param1", "global_var1")
        self._check_block_id_is_the_same(lib, "param1", "global_var2")

    def test185CheckAssignToSeveralGlobalVars2(self):
        lib = self._generic_test("unitest/test185.c", [ (crust.crust.MSG_ERROR, "At exit point in line {:d}, global variable '{:s}' points to the same block than global variable '{:s}'.", 20, "global_var1", "global_var2") ])
        self._check_block_id_is_the_same(lib, "param1", "global_var1")
        self._check_block_id_is_the_same(lib, "param1", "global_var2")

    def test186CheckIfWithUninitializedVariable(self):
        self._generic_test("unitest/test186.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "data", 7, 11) ])

    def test187GlobalVarsAlwaysNotNull(self):
        self._all_fine_test("unitest/test187.c")

    def test188GlobalAssignment(self):
        self._generic_test("unitest/test188.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 10) ])

    def test189AssignmentWithAsterisk(self):
        lib = self._all_fine_test("unitest/test189.c")
        self._check_var_state(lib, "var1", crust.crust.VALUE_NOT_NULL_OR_NULL)
        self._check_var_state(lib, "var2", crust.crust.VALUE_NOT_NULL_OR_NULL)

    def test190FunctionWithVoidParameter(self):
        self._all_fine_test("unitest/test190.c")

    def test191CheckAssignmentAfterAssignmentToGlobal(self):
        lib = self._all_fine_test("unitest/test191.c")
        self._check_block_id_is_the_same(lib, "arg1", "var1", 0)
        self._check_block_id_is_not_same(lib, "arg1", "var1", 1)


    def test192GlobalVariableFreedAtExit(self):
        self._generic_test("unitest/test192.c", [ (crust.crust.MSG_ERROR, "At exit point in line {:d}, global variable '{:s}' points to a block freed at line {:d}.", 14, "var1", 12) ])

    def test193GlobalVariableFreedAtExit(self):
        lib = self._generic_test("unitest/test193.c", [ (crust.crust.MSG_ERROR, "At exit point in line {:d}, global variable '{:s}' points to the same block than global variable '{:s}'.", 15, "var1", "var2") ])
        self._check_block_id_is_the_same(lib, "arg1", "var1")
        self._check_block_id_is_the_same(lib, "arg1", "var2")

    def test194GlobalVariableFreedAtExit(self):
        lib = self._all_fine_test("unitest/test194.c")
        self._check_var_state(lib, "global_var", crust.crust.VALUE_NOT_NULL_OR_NULL)

    def test195ForLoopWithoutFreeing(self):
        self._generic_test("unitest/test195.c", [ (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", "var", 12, 9), (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", "var", 12, 12) ])

    def test196ForLoopWithoutFreeing2(self):
        self._generic_test("unitest/test196.c", [ (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", "var", 14, 11), (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", "var", 14, 14) ])

    def test197ForLoopFreeing(self):
        self._all_fine_test("unitest/test197.c")

    def test198ForLoopWithoutFreeingWithAlias(self):
        self._generic_test("unitest/test198.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "var", 13, 19) ])

    def test199AssingFunctionPointerToAlias(self):
        self._generic_test("unitest/test199.c", [ (crust.crust.MSG_ERROR, "Trying to assign the function '{:s}' into the variable '{:s}', which is not a pointer to function, at line {:d}", "function2", "tmp1", 16), (crust.crust.MSG_ERROR, "Assigning a pointer of the function '{:s}' to the alias '{:s}' at line {:d}", "function2", "tmp1", 16) ])

    def test200GlobalVariableFreedAtExit2(self):
        lib = self._all_fine_test("unitest/test200.c")
        self._check_var_state(lib, "global_var", crust.crust.VALUE_NOT_NULL_OR_NULL)

    def test201TestSetNull(self):
        lib = self._all_fine_test("unitest/test201.c")
        self._check_var_state(lib, "var1", crust.crust.VALUE_NOT_NULL_OR_NULL, 0)
        self._check_var_state(lib, "var1", crust.crust.VALUE_NULL, 1)

    def test202TestSetNotNull(self):
        lib = self._generic_test("unitest/test202.c", [ (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, but it is not freed", "var1", 13) ])
        self._check_var_state(lib, "var1", crust.crust.VALUE_NOT_NULL_OR_NULL, 0)
        self._check_var_state(lib, "var1", crust.crust.VALUE_NOT_NULL, 1)

    def test203TestSetNullNonGlobal(self):
        lib = self._generic_test("unitest/test203.c", [ (crust.crust.MSG_CRITICAL, "Trying to set to NULL the state of the non-global variable '{:s}' at line {:d}", "var1", 7) ])

    def test204TestSetNullNonGlobal(self):
        lib = self._generic_test("unitest/test204.c", [ (crust.crust.MSG_CRITICAL, "Trying to set to NOT_NULL the state of the non-global variable '{:s}' at line {:d}", "var1", 7) ])

    def test205SwitchWithArrayItem(self):
        self._all_fine_test("unitest/test205.c")

    def test206SwitchWithPointerContent(self):
        self._all_fine_test("unitest/test206.c")

    def test207SwitchWithPointerContent(self):
        lib = self._all_fine_test("unitest/test207.c")
        self._check_var_state(lib, "str", crust.crust.VALUE_NOT_NULL)

    def test208TestSetNullNonGlobal(self):
        lib = self._generic_test("unitest/test208.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param2", 7, 15), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "param1", 7, 15) ])
        self._check_var_state(lib, "a", crust.crust.VALUE_NOT_NULL_OR_NULL)

    def test209TestSetNullNonGlobal(self):
        lib = self._generic_test("unitest/test209.c", [ (crust.crust.MSG_CRITICAL, "Function definition for '{:s}' at line {:d}, file '{:s}' differs from definition at line {:d}, file '{:s}'", "function", 9, "unitest/test209.c", 6, "unitest/test209.c") ])

    def test210UseFreedTypedefNotPointer(self):
        self._generic_test("unitest/test210.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "function", 6, 5) ])

    def test211CheckTypedefEnums(self):
        self._all_fine_test("unitest/test211.c")

    def test212InitializedArrays(self):
        self._all_fine_test("unitest/test212.c")

    def test213InitializedNoSizeArrays(self):
        self._all_fine_test("unitest/test213.c")

    def test214InitializeStructs(self):
        self._all_fine_test("unitest/test214.c")

    def test215TestFree(self):
        self._all_fine_test("unitest/test215.c")

    def test216TestRealloc(self):
        self._generic_test("unitest/test216.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 15) ])

    def test217TestRealloc2(self):
        self._generic_test("unitest/test217.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 17), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "var2", 17, 18) ])

    def test218TestMalloc(self):
        self._generic_test("unitest/test218.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 15) ])

    def test219TestMalloc2(self):
        self._generic_test("unitest/test219.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 17), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "var", 17, 18) ])

    def test220TestCalloc(self):
        self._generic_test("unitest/test220.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 15) ])

    def test221TestCalloc2(self):
        self._generic_test("unitest/test221.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 17), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "var", 17, 18) ])

    def test222TestConditionTrue(self):
        self._generic_test("unitest/test222.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "var", 11, 21) ])

    def test223TestOverrideSignature(self):
        self._all_fine_test("unitest/test223.c")

    def test224TestOverrideSignature2(self):
        self._all_fine_test("unitest/test224.c")

    def test225TestNotOverrideSignature(self):
        self._generic_test("unitest/test225.c", [ (crust.crust.MSG_CRITICAL, "Function definition for '{:s}' at line {:d}, file '{:s}' differs from definition at line {:d}, file '{:s}'", "test_function", 4, "unitest/test225.c", 3, "unitest/test225.c"), (crust.crust.MSG_CRITICAL, "Expected a non __crust__ variable as argument {:d} when calling function '{:s}' at line {:d}, but passed a __crust__ variable", 1, "test_function", 9) ])

    def test226TestNotOverrideSignature2(self):
        self._generic_test("unitest/test226.c", [ (crust.crust.MSG_CRITICAL, "Function definition for '{:s}' at line {:d}, file '{:s}' differs from definition at line {:d}, file '{:s}'", "test_function", 4, "unitest/test226.c", 3, "unitest/test226.c") ])

    def test227TestAutoIncrement(self):
        self._generic_test("unitest/test227.c", [ (crust.crust.MSG_CRITICAL, "Using autoincrement with __crust__ pointer '{:s}' at line {:d}", "param", 5), (crust.crust.MSG_CRITICAL, "Using autodecrement with __crust__ pointer '{:s}' at line {:d}", "param2", 6) ])

    def test228AssignGlobalVarNotNull(self):
        self._generic_test("unitest/test228.c", [ (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, but it is not freed", "var1", 10) ])

    def test229UseGlobalVarNotNull(self):
        self._generic_test("unitest/test229.c", [ (crust.crust.MSG_ERROR, "At exit point in line {:d}, global variable '{:s}' points to a block freed at line {:d}.", 16, "var1", 11) ])

    def test230SetGlobalToNullAfterUse(self):
        self._generic_test("unitest/test230.c", [ (crust.crust.MSG_WARNING, "Setting to NULL the state of the already accessed global variable '{:s}' at line {:d}", "var1", 10) ])

    def test231SetGlobalToNullAfterUse(self):
        self._generic_test("unitest/test231.c", [ (crust.crust.MSG_WARNING, "Setting to NOT_NULL the state of the already accessed global variable '{:s}' at line {:d}", "var1", 10) ])

    def test232SetGlobalToNullInSeveralPaths(self):
        self._all_fine_test("unitest/test232.c")

    def test233CompareWithCrustPointerAlone(self):
        self._all_fine_test("unitest/test233.c")

    def test234CompareWithCrustPointerAlone2(self):
        self._all_fine_test("unitest/test234.c")

    def test235CompareWithCrustPointerAlone3(self):
        self._generic_test("unitest/test235.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "p", 5, 10) ])

    def test236CompareWithCrustPointerAlone4(self):
        self._generic_test("unitest/test236.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", "p", 5, 12) ])

    def test237CompareWithCrustPointerAloneUninitialized(self):
        self._generic_test("unitest/test237.c", [ (crust.crust.MSG_ERROR, "Using uninitialized variable '{:s}' at line {:d}", "p", 7) ])

    def test238CompareWithCrustPointerAloneFreed(self):
        self._all_fine_test("unitest/test238.c")

    def test239NullCrustPointerIsNotFreed(self):
        self._all_fine_test("unitest/test239.c")

    def test240CompareWithCrustPointerAloneFreedNotNull(self):
        self._all_fine_test("unitest/test240.c")

    def test241CompareFreedWithNull(self):
        self._all_fine_test("unitest/test241.c")

    def test242CompareWithCrustPointerWithNullUninitialized(self):
        self._generic_test("unitest/test242.c", [ (crust.crust.MSG_ERROR, "Using uninitialized variable '{:s}' at line {:d}", "p", 7) ])

    def test243ReturnVoidPointer(self):
        self._all_fine_test("unitest/test243.c")

    def test244ReturnVoidPointer2(self):
        self._generic_test("unitest/test244.c", [ (crust.crust.MSG_ERROR, "Function '{:s}' expects a return value, but the code exited without it", 'function_name') ])

    def test245AssignBorrowedToBorrowed(self):
        self._all_fine_test("unitest/test245.c")

    def test246ReturnBorrowedVariable(self):
        self._all_fine_test("unitest/test246.c")

    def test247ReturnBorrowedParameter(self):
        self._all_fine_test("unitest/test247.c")

    def test248ReturnUninitializedBorrowedParameter(self):
        self._generic_test("unitest/test248.c", [ (crust.crust.MSG_ERROR, "Returning variable '{:s}' at line {:d} is uninitialized", 'tmp', 8) ])

    def test249NoVoidOnEmptyParameterFunction(self):
        self._generic_test("unitest/test249.c", [ (crust.crust.MSG_ERROR, "Function '{:s}' at line {:d} has no parameters (must have 'void' inside the parentheses)", 'main', 9) ])

    def test250NoVoidOnEmptyParameterFunctionDefinition(self):
        self._generic_test("unitest/test250.c", [ (crust.crust.MSG_ERROR, "Function '{:s}' at line {:d} has no parameters (must have 'void' inside the parentheses)", 'function', 9) ])
    def test251NoVoidOnEmptyParameterFunctionAndDefinitionInHeader(self):
        self._generic_test("unitest/test251.c", [ (crust.crust.MSG_ERROR, "Function '{:s}' at line {:d} in file '{:s}' has no parameters (must have 'void' inside the parentheses)", 'function', 4, 'unitest/test251.h'), (crust.crust.MSG_ERROR, "Function '{:s}' at line {:d} has no parameters (must have 'void' inside the parentheses)", 'function', 12) ])

    def test252switchWithoutDefault(self):
        self._generic_test("unitest/test252.c", [ (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", 'param', 5, 19) ])

    def test253ReturnNULLBorrowed(self):
        self._all_fine_test("unitest/test253.c")

    def test254CallingPossibleNullPointerFunction(self):
        self._generic_test("unitest/test254.c", [ (crust.crust.MSG_WARNING, "Using function pointer '{:s}' at line {:d} with a possible NULL value", 'cb', 2)])

    def test255CallingNotNullPointerFunction(self):
        self._all_fine_test("unitest/test255.c")

    def test256ReturnCallingNOtNullPointerFunctionWithNull(self):
        self._generic_test("unitest/test256.c", [ (crust.crust.MSG_ERROR, "Passing NULL as argument {:d} when calling function '{:s}' at line {:d}, but it must be a not_null value", 1, 'function', 6)])

    def test257InfiniteWhileWithReturn(self):
        self._all_fine_test("unitest/test257.c")

    def test258InfiniteWhile(self):
        self._all_fine_test("unitest/test258.c")

    def test259InfiniteWhile2(self):
        self._all_fine_test("unitest/test259.c")

    def test260InfiniteWhileInsideSwithInterleaved(self):
        self._all_fine_test("unitest/test260.c")

    def test261ForInsideSwithInterleaved(self):
        self._all_fine_test("unitest/test261.c")

    def test262InfiniteWhileInsideSwithInterleavedDefaultAfter(self):
        self._all_fine_test("unitest/test262.c")

    def test263InfiniteWhileInsideSwithInterleavedDefaultAfterAndNestedBlocks(self):
        self._all_fine_test("unitest/test263.c")

    def test264NoWarningOnWarning(self):
        self._all_fine_test("unitest/test264.c")

    def test265NoWarningOnError(self):
        self._generic_test("unitest/test265.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} was freed at line {:d}", 1, "main", 9, 8) ])

    def test266NoWarningOnWarningAfterReturn(self):
        self._all_fine_test("unitest/test266.c")

    def test267AllowSetNullOnStaticVariables(self):
        self._all_fine_test("unitest/test267.c")

    def test268AllowSetNotNullOnStaticVariables(self):
        self._all_fine_test("unitest/test268.c")

    def test269StaticVariableWithFreedBlock(self):
        self._generic_test("unitest/test269.c", [ (crust.crust.MSG_ERROR, "At exit point in line {:d}, static variable '{:s}' points to a block freed at line {:d}.", 10, "p", 9) ])

    def test270AssignStaticNullVariableWithBlock(self):
        self._all_fine_test("unitest/test270.c")

    def test271AssignStaticNotNullVariableWithBlock(self):
        self._generic_test("unitest/test271.c", [ (crust.crust.MSG_ERROR, "Assignment to '{:s}' at line {:d}, but it is not freed", "p", 9) ])

    def test272DufDevice1(self):
        self._generic_test("unitest/test272.c", [ (crust.crust.MSG_ERROR, "Argument {:d} when calling function '{:s}' at line {:d} isn't initialized", 1, "function2", 20) ])

    def test273CheckDuffDevice2(self):
        self._all_fine_test("unitest/test273.c")

    def test274NoWarningOnCrustToCrustCast(self):
        self._all_fine_test("unitest/test274.c")

    def test275NoWarningOnNonCrustToNonCrustCast(self):
        self._all_fine_test("unitest/test275.c")

    def test276WarningOnCrustToNonCrustCast(self):
        self._generic_test("unitest/test276.c", [ (crust.crust.MSG_WARNING, "Typecasting a CRUST value into a non-CRUST type at line {:d}", 6), (crust.crust.MSG_ERROR, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", 'v', 3, 7) ])

    def test277WarningOnCrustToNonCrustCast(self):
        self._generic_test("unitest/test277.c", [ (crust.crust.MSG_WARNING, "Typecasting a non-CRUST value into a CRUST type at line {:d}", 6) ])

if __name__ == '__main__':
    try:
        os.remove("error_list.txt")
    except:
        pass
    unittest.main()
