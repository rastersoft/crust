#!/usr/bin/env python3

# Copyright 2017 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of CRUST
#
# CRUST is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License.
#
# CRUST is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


class crust_definitions(object):

    version = "0.13"

    # when a variable hasn't been assigned yet
    VALUE_UNINITIALIZED = 0
    # when a variable has been initialized to a non NULL value
    VALUE_NULL = 1
    # when a variable has been initialized to a NULL value
    VALUE_NOT_NULL = 2
    # when a variable has been initialized to a value that can be NULL or not NULL
    VALUE_NOT_NULL_OR_NULL = 3
    # when a variable NOT NULL is presumed to have been freed after have been passed on to a function
    VALUE_FREED = 4
    # when a variable NULL_OR_NOT_NULL is presumed to have been freed after have been passed on to a function
    VALUE_FREED_OR_NULL = 5
    # the variable is global, so it is presumed that, when used, it is INITIALIZED_OR_NULL, and when assigned is FREED
    VALUE_GLOBAL = 6
    # the variable is a function
    VALUE_FUNCTION = 7

    CONDITION_FALSE      = 0
    CONDITION_TRUE       = 1
    CONDITION_FALSE_TRUE = 2

    MSG_NORMAL   = 0
    MSG_WARNING  = 1
    MSG_ERROR    = 2
    MSG_CRITICAL = 3

    TYPE_NO_MATTER = 0
    TYPE_NO_CRUST  = 1
    TYPE_CRUST     = 2

    state_str = ["VALUE_UNINITIALIZED", "VALUE_NULL", "VALUE_NOT_NULL", "VALUE_NOT_NULL_OR_NULL", "VALUE_FREED", "VALUE_FREED_OR_NULL", "VALUE_GLOBAL", "VALUE_FUNCTION" ]
