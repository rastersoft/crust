#!/usr/bin/env python3

class tokens(object):
    token_list = {
        258:'IDENTIFIER',
        259:'CONSTANT',
        260:'STRING_LITERAL',
        261:'SIZEOF',
        262:'PTR_OP',
        263:'INC_OP',
        264:'DEC_OP',
        265:'LEFT_OP',
        266:'RIGHT_OP',
        267:'LE_OP',
        268:'GE_OP',
        269:'EQ_OP',
        270:'NE_OP',
        271:'AND_OP',
        272:'OR_OP',
        273:'MUL_ASSIGN',
        274:'DIV_ASSIGN',
        275:'MOD_ASSIGN',
        276:'ADD_ASSIGN',
        277:'SUB_ASSIGN',
        278:'LEFT_ASSIGN',
        279:'RIGHT_ASSIGN',
        280:'AND_ASSIGN',
        281:'XOR_ASSIGN',
        282:'OR_ASSIGN',
        283:'TYPE_NAME',
        284:'TYPEDEF',
        285:'EXTERN',
        286:'STATIC',
        287:'AUTO',
        288:'REGISTER',
        289:'INLINE',
        290:'RESTRICT',
        291:'CHAR',
        292:'SHORT',
        293:'INT',
        294:'LONG',
        295:'SIGNED',
        296:'UNSIGNED',
        297:'FLOAT',
        298:'DOUBLE',
        299:'CONST',
        300:'VOLATILE',
        301:'VOID',
        302:'BOOL',
        303:'COMPLEX',
        304:'IMAGINARY',
        305:'STRUCT',
        306:'UNION',
        307:'ENUM',
        308:'ELLIPSIS',
        309:'CASE',
        310:'DEFAULT',
        311:'IF',
        312:'ELSE',
        313:'SWITCH',
        314:'WHILE',
        315:'DO',
        316:'FOR',
        317:'GOTO',
        318:'CONTINUE',
        319:'BREAK',
        320:'RETURN',
        321:'VA_LIST',
        322:'T_EXTENSION',
        323:'TYPE_SPECIFIER',
        324:'VARIABLE_DEFINITION',
        325:'T_NULL',
        326:'FCONSTANT',
        327:'EMPTY_DECLARATOR',
        328:'FUNCTION_CALL',
        329:'FUNCTION_DEFINITION',
        330:'FUNCTION_DECLARATION',
        331:'BRACKETS_CONTAINER',
        332:'LABEL',
        333:'START_BLOCK',
        334:'END_BLOCK',
        335:'TYPECAST',
        336:'TYPENAME_IDENTIFIER',
        337:'T_TYPEOF',
        338:'CRUST_T',
        339:'CRUST_BORROW',
        340:'CRUST_RECYCLE',
        341:'CRUST_ALIAS',
        342:'CRUST_NOT_NULL',
        343:'CRUST_ENABLE',
        344:'CRUST_DISABLE',
        345:'CRUST_FULL_ENABLE',
        346:'CRUST_DEBUG',
        347:'CRUST_NO_ZERO',
        348:'CRUST_SET_NULL',
        349:'CRUST_SET_NOT_NULL',
        350:'CRUST_OVERRIDE',
        351:'CRUST_NO_WARNING',
        352:'OWN_POINTER',
        353:'ASSIGN_BLOCK',
        'IDENTIFIER':258,
        'CONSTANT':259,
        'STRING_LITERAL':260,
        'SIZEOF':261,
        'PTR_OP':262,
        'INC_OP':263,
        'DEC_OP':264,
        'LEFT_OP':265,
        'RIGHT_OP':266,
        'LE_OP':267,
        'GE_OP':268,
        'EQ_OP':269,
        'NE_OP':270,
        'AND_OP':271,
        'OR_OP':272,
        'MUL_ASSIGN':273,
        'DIV_ASSIGN':274,
        'MOD_ASSIGN':275,
        'ADD_ASSIGN':276,
        'SUB_ASSIGN':277,
        'LEFT_ASSIGN':278,
        'RIGHT_ASSIGN':279,
        'AND_ASSIGN':280,
        'XOR_ASSIGN':281,
        'OR_ASSIGN':282,
        'TYPE_NAME':283,
        'TYPEDEF':284,
        'EXTERN':285,
        'STATIC':286,
        'AUTO':287,
        'REGISTER':288,
        'INLINE':289,
        'RESTRICT':290,
        'CHAR':291,
        'SHORT':292,
        'INT':293,
        'LONG':294,
        'SIGNED':295,
        'UNSIGNED':296,
        'FLOAT':297,
        'DOUBLE':298,
        'CONST':299,
        'VOLATILE':300,
        'VOID':301,
        'BOOL':302,
        'COMPLEX':303,
        'IMAGINARY':304,
        'STRUCT':305,
        'UNION':306,
        'ENUM':307,
        'ELLIPSIS':308,
        'CASE':309,
        'DEFAULT':310,
        'IF':311,
        'ELSE':312,
        'SWITCH':313,
        'WHILE':314,
        'DO':315,
        'FOR':316,
        'GOTO':317,
        'CONTINUE':318,
        'BREAK':319,
        'RETURN':320,
        'VA_LIST':321,
        'T_EXTENSION':322,
        'TYPE_SPECIFIER':323,
        'VARIABLE_DEFINITION':324,
        'T_NULL':325,
        'FCONSTANT':326,
        'EMPTY_DECLARATOR':327,
        'FUNCTION_CALL':328,
        'FUNCTION_DEFINITION':329,
        'FUNCTION_DECLARATION':330,
        'BRACKETS_CONTAINER':331,
        'LABEL':332,
        'START_BLOCK':333,
        'END_BLOCK':334,
        'TYPECAST':335,
        'TYPENAME_IDENTIFIER':336,
        'T_TYPEOF':337,
        'CRUST_T':338,
        'CRUST_BORROW':339,
        'CRUST_RECYCLE':340,
        'CRUST_ALIAS':341,
        'CRUST_NOT_NULL':342,
        'CRUST_ENABLE':343,
        'CRUST_DISABLE':344,
        'CRUST_FULL_ENABLE':345,
        'CRUST_DEBUG':346,
        'CRUST_NO_ZERO':347,
        'CRUST_SET_NULL':348,
        'CRUST_SET_NOT_NULL':349,
        'CRUST_OVERRIDE':350,
        'CRUST_NO_WARNING':351,
        'OWN_POINTER':352,
        'ASSIGN_BLOCK':353,
    }

    @staticmethod
    def get_token(token):
        if isinstance(token,int):
            if token < 258:
                return "{:c}".format(token)
            else:
                return tokens.token_list[token]
        else:
            return token